<?
function sendSms($sender, $recipients, $message, $priceGroup, $flash)
{
    $CID = "4873";
    $password = "dinliving";
    
    if (!is_numeric($priceGroup) || !is_numeric($flash))
        return "priceGroup and flash must be a number";

    $serverRequest = "http://api.sendega.com/SendSMS.asp";
    $serverRequest .= "?CID=".$CID;
    $serverRequest .= "&Password=".$password;
    if (is_numeric($sender))
        $serverRequest .= "&fromNumber=".$sender;
    else
        $serverRequest .= "&fromAlpha=".urlencode($sender);
    $serverRequest .= "&recipient=".$recipients;
    $serverRequest .= "&Msg=".urlencode($message);
    $serverRequest .= "&priceGroup=".$priceGroup;
    $serverRequest .= "&flash=".$flash;
    
    $serverResult = file_get_contents($serverRequest);
    
    if (!$serverResult)
         return "Returned error while trying to connect to Sendega";

    $xml = simplexml_load_string($serverResult);
    
    if ($xml->success == "true")
        $result = $xml->msgid;
    else
        $result = $xml->errormsg;

    return $result;
}
?>