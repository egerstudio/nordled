<script src="../js_font/cufon-yui.js" type="text/javascript"></script>
<script src="../js_font/district.font.js" type="text/javascript"></script>
<script src="../js_font/calibri.font.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="../js/prototype.js"></script>-->
<!--<script type="text/javascript" src="../js/scriptaculous.js?load=effects,builder"></script>-->
<script type="text/javascript" src="../js/jquery-1.5.min.js"></script>
<script type="text/javascript" src="../js/jquery.cycle.all.min.js"></script>




<script type="text/javascript">
			Cufon.replace('a.navlink', {
				hover: true, fontFamily: 'Calibri'
				}
			);
			Cufon.replace('h1', {fontFamily: 'District'});
			Cufon.replace('h2,h3, #footer p, .toptext, p.frontsplash', {fontFamily: 'Calibri'});
</script>


<script type="text/javascript">
$(document).ready(function() {
    $('.slideshow').cycle(
	{
		fx: 		'fade',
		sync:		0,
		speed: 		100,
		timeout:	 4000,
		cleartype: true,
		cleartypeNoBg: true
	}
	);
});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-20927943-5']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
