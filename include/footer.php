<div id="footer">
	<div style="width: 50%; float: left; padding-left: 10px;">
    <p>
    	LED-shop Norge &copy; 2017. Alle rettigheter reservert.
      	<br />Med forbehold om trykkfeil og prisendringer.
        <br />
        Alle priser er inkludert mva.
      <br />
<br />

       <a href="#top">Til toppen av siden</a>
    </p>
    </div>
    <div id="bottominfo"><p>Besøk oss:<br />LED-shop Norge<br />Busundveien 61<br />3519 Hønefoss<br /><br />Telefon: (+47) 958 66 862</p></div>
</div>
