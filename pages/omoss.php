<?php
$page = "omoss";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LED-shop Norge</title>
<link href="../css/common.css" rel="stylesheet" type="text/css" />
<?php require("../include/javascripts.php");?>
</head>
<body>
<div id="wrap">

    <div id="page">
    	<?php require("../include/top.php");?>

        <div id="content">
        	<h1>Om oss</h1>
            <div class="entry">
            	<h2>Kontaktinfo</h2>
                <p style="font-size: 18px;">
                Kontakt oss på e-post: <a style="font-weight: bold;" href="mailto:info@nordled.no">info@nordled.no</a>
                </p>

	           <h2>Postadresse</h2>
               <p>
               <strong>LED-shop Norge</strong><br />
               Postboks 3043, N-3501 Hønefoss, Norway <br/>
               eller
               <br />
               <strong>LED-shop Norge</strong><br />
               Busundveien 61, 3519 Hønefoss, Norway

                </p>
                <h2>Varelevering</h2>
                <p>
                LED-shop Norge<br />
				Busundveien 61, 3519 Hønefoss, Norway
				</p>
                <h2>Telefon</h2>
                <p>
                Mobil: +47 958 66 862
                </p>
                <h2>Salgs- og leveringsbetingelser</h2>
                <p>
                Alle forsendelser fra oss sendes med Posten i oppkrav om ikke annet avtales på forhånd.<br />
                Uavhentede pakker faktureres konsekvent med et gebyr på kr. 550,-.
                <br /><br />

                </p>
                <h2>Annen informasjon</h2>
                <p>
                Orgnr: 986 381 074mva<br />
<br />
				Kontaktperson: Odd Ragnar Karlsen<br />
				E-post: <a href="mailto:info@nordled.no">info@nordled.no</a>
                <br />
                Web: <a href="http://www.led-shopnorge.com/">www.led-shopnorge.com</a> / <a href="http://www.nordled.no">www.nordled.no</a>
                </p>




          	</div>

            <div class="entry">
            <h2>Her finner du oss</h2>
            <iframe width="840" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=busundveien+61+h%C3%B8nefoss&amp;aq=&amp;sll=37.0625,-95.677068&amp;sspn=55.060677,135.263672&amp;vpsrc=6&amp;ie=UTF8&amp;hq=&amp;hnear=Busundveien+61,+3519,+Ringerike,+Buskerud,+Norway&amp;t=m&amp;ll=60.195132,10.290756&amp;spn=0.136518,0.54863&amp;z=11&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=busundveien+61+h%C3%B8nefoss&amp;aq=&amp;sll=37.0625,-95.677068&amp;sspn=55.060677,135.263672&amp;vpsrc=6&amp;ie=UTF8&amp;hq=&amp;hnear=Busundveien+61,+3519,+Ringerike,+Buskerud,+Norway&amp;t=m&amp;ll=60.195132,10.290756&amp;spn=0.136518,0.54863&amp;z=11&amp;iwloc=A" style="color:#0000FF;text-align:left">Vis større kart</a></small>
            </div>


        </div><!-- content end-->
	</div>
</div>

<?php require("../include/footer.php");?>


<script type="text/javascript"> Cufon.now(); </script>

</body>
</html>
