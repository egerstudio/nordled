<?php
$page = "protop";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LED-shop Norge</title>
<link href="../css/common.css" rel="stylesheet" type="text/css" />
<?php require("../include/javascripts.php");?>

<script src="../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/lightbox.js"></script>

<link href="../css/lightbox2.css" rel="stylesheet" />
</head>
<body>
<div id="wrap">
	
    <div id="page">
    	<?php require("../include/top.php");?>
		
        <div id="content">
        	<h1>Bil- &amp; tilhengerpåbygg</h1>
            <div class="entry">
                 
	           <h2>"Vi bygger på alt som ruller"</h2>
               <p>
               <strong>NYHET! Vi forhandler produkter fra svenske <a href="http://www.snab.se" target="_blank">SN fordonsinredning</a>, og leverer klart for montering til kunder i Norge!</strong>
               <br />
              <br />
              <strong>Klikk på forhåndsvisningene for større bilde.</strong>
              <br />
              
              <a href="../images/protop/001.jpg" rel="lightbox[protop]" title="Bilde 001"><img src="../images/protop/001s.jpg" /></a>
              <a href="../images/protop/002.jpg" rel="lightbox[protop]" title="Bilde 002"><img src="../images/protop/002s.jpg" /></a>
              <a href="../images/protop/003.jpg" rel="lightbox[protop]" title="Bilde 003"><img src="../images/protop/003s.jpg" /></a>
              <a href="../images/protop/004.jpg" rel="lightbox[protop]" title="Bilde 004"><img src="../images/protop/004s.jpg" /></a>
              <a href="../images/protop/005.jpg" rel="lightbox[protop]" title="Bilde 005"><img src="../images/protop/005s.jpg" /></a>
              <a href="../images/protop/006.jpg" rel="lightbox[protop]" title="Bilde 006"><img src="../images/protop/006s.jpg" /></a>
              <a href="../images/protop/007.jpg" rel="lightbox[protop]" title="Bilde 007"><img src="../images/protop/007s.jpg" /></a> 
              <a href="../images/protop/008.jpg" rel="lightbox[protop]" title="Bilde 008"><img src="../images/protop/008s.jpg" /></a>
              <a href="../images/protop/009.jpg" rel="lightbox[protop]" title="Bilde 009"><img src="../images/protop/009s.jpg" /></a>
              <a href="../images/protop/010.jpg" rel="lightbox[protop]" title="Bilde 010"><img src="../images/protop/010s.jpg" /></a>
              <a href="../images/protop/011.jpg" rel="lightbox[protop]" title="Bilde 011"><img src="../images/protop/011s.jpg" /></a> 
              <a href="../images/protop/012.jpg" rel="lightbox[protop]" title="Bilde 012"><img src="../images/protop/012s.jpg" /></a>
              <a href="../images/protop/013.jpg" rel="lightbox[protop]" title="Bilde 013"><img src="../images/protop/013s.jpg" /></a>

          	</div>
            
            
         
            <div class="entry">
                 
	           <h2>ProTop Standard W1</h2>
                  <img src="../images/produkter/protopstandardw1.jpg" style="float: left; padding-right: 20px;" />
               
          	</div>
            
            <div class="entry">
                 
	           <h2>ProTop Standard W2</h2>
               <img src="../images/produkter/protopstandard-w2.jpg" style="float: left; padding-right: 20px;" />
               <p style="float: left;">
               Skapet er bygd opp av sandwichpaneler med høyblank overflate, leveres med standard hvit RAL 9016 farge, og elokserte aluminiumsprofiler.                
               Skapet har 3 dører, kantkledd med aluminiumsprofiler. Dørene er helt tette og hengslet i overkant.
               <br />
               <br />
               Bunnen er oppbygd av aluminiumsprofiler med sklisikker plywood. Manuell eller elektrisk lås på dørene, og høyt plassert bremselys.
               Skapet kan tilpasses samtlige kjøretøy.
               <br /><br />

               Skapet leveres både til dobbeltcab og 1 1/2 cab.
               <br /><br />
               Vi leverer til blant annet Ford, Isuzu, Mercedes-Benz, Mitsubishi, Nissan m. fl.
               
               </p>
          	</div>
            
            
             
        </div><!-- content end-->
	</div>
</div>

<?php require("../include/footer.php");?>


<script type="text/javascript"> Cufon.now(); </script>

</body>
</html>
