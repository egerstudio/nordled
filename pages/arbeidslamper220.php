<?php
$page = "arbeidslamper2";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LED-shop Norge</title>
<link href="../css/common.css" rel="stylesheet" type="text/css" />
<?php require("../include/javascripts.php");?>
</head>
<body>
<div id="wrap">

    <div id="page">
    	<?php require("../include/top.php");?>

        <div id="content">
        	<h1>Arbeidslys 220V</h1>
            <h2>alle priser er inklusiv merverdiavgift</h2>





          <!--<div class="entryhalf">
          	<h2>NordLED fasadelys 100W</h2>
         	  <p class="productinfo">
              <img src="../images/produkter/3001601.jpg" class="left" style="border: 0;" />
              Slank og fleksibel fasadebelysning i værbestandig utførelse.
              <br />
				100-277V, 1.2A
			</p>
              <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header" style="text-align: right;">Pris</span></h4>
             <h4 class="pricesmall"><span class="art">3001601</span><span class="color">sort</span><span class="volt" style="text-align: right;">1 980.00</span></h4>
             <h4 class="pricesmall"><span class="art">3001602</span><span class="color">hvit</span><span class="volt" style="text-align: right;">1 980.00</span></h4>
             <h4 class="spec">&nbsp;</h4>
          </div>


          <div class="entryhalf">
          	<h2>NordLED fasadelys 200W</h2>
         	  <p class="productinfo">
              <img src="../images/produkter/3001603.jpg" class="left" style="border: 0;" />
              Slank og fleksibel fasadebelysning i værbestandig utførelse.
              <br />
				100-277V, 2.3A
			</p>
              <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header" style="text-align: right;">Pris</span></h4>
             <h4 class="pricesmall"><span class="art">3001603</span><span class="color">sort</span><span class="volt" style="text-align: right;">2 480.00</span></h4>
             <h4 class="pricesmall"><span class="art">3001604</span><span class="color">hvit</span><span class="volt" style="text-align: right;">2 480.00</span></h4>
             <h4 class="spec">&nbsp;</h4>
          </div>

          <div class="entryhalf">
          	<h2>NordLED fasadelys 300W</h2>
         	  <p class="productinfo">
              <img src="../images/produkter/3001605.jpg" class="left" style="border: 0;" />
              Slank og fleksibel fasadebelysning i værbestandig utførelse.
              <br />
				100-277V, 3.4A
			</p>
              <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header" style="text-align: right;">Pris</span></h4>
             <h4 class="pricesmall"><span class="art">3001605</span><span class="color">sort</span><span class="volt" style="text-align: right;">3 080.00</span></h4>
             <h4 class="pricesmall"><span class="art">3001606</span><span class="color">hvit</span><span class="volt" style="text-align: right;">3 080.00</span></h4>
             <h4 class="spec">&nbsp;</h4>
          </div>


          <div class="entryhalf">
          	<h2>NordLED fasadelys 400W</h2>
         	  <p class="productinfo">
              <img src="../images/produkter/3001605.jpg" class="left" style="border: 0;" />
              Slank og fleksibel fasadebelysning i værbestandig utførelse.
              <br />
				100-277V, 4.5A
			</p>
              <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header" style="text-align: right;">Pris</span></h4>
             <h4 class="pricesmall"><span class="art">3001607</span><span class="color">sort</span><span class="volt" style="text-align: right;">3 880.00</span></h4>
             <h4 class="pricesmall"><span class="art">3001608</span><span class="color">hvit</span><span class="volt" style="text-align: right;">3 880.00</span></h4>
             <h4 class="spec">&nbsp;</h4>
          </div>

				-->


        <div class="entryhalf">
              <h2>NordLED arbeidslys 20W</h2>
              <p class="productinfo">
                <img src="../images/produkter/6001-020-1.jpg" class="left" style="border: 0;" />
               Arbeidslys 20W i aluminiumshus og herdet glass, værbestandig. IP65. Sammenlignet med tradisjonell arbeidsbelysning sparer man inntil 60% energi, levetid over 30.000 timer. RoHS og CE merket. Hvitt lys.<br  /><br  /></p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">6001020</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 920.00</span></h4>
               <h4 class="spec">-</h4>
            </div>

            <!--
            <div class="entryhalf">
              <h2>NordLED arbeidslys 30W</h2>
              <p class="productinfo">
                <img src="../images/produkter/4001215.jpg" class="left" style="border: 0;" />
                Arbeidslys 30W i aluminiumshus og herdet glass, værbestandig. IP65. Sammenlignet med tradisjonell arbeidsbelysning sparer man inntil 60% energi, levetid over 30.000 timer. RoHS og CE merket. Hvitt lys.<br  /><br  />
                Forbruk 0.4Amp, 100-240V.
                <br />
        3 130lm, 100&deg; spredning.
                <br /><br />


                </p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">6001030</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 880.00</span></h4>
               <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys30w.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
            </div>
            -->



            <div class="entryhalf">
              <h2>NordLED arbeidslys 50W</h2>
              <p class="productinfo">
                <img src="../images/produkter/6001-050-1.jpg" class="left" style="border: 0;" />
                Arbeidslys 50W i aluminiumshus og herdet glass, værbestandig. IP65. Sammenlignet med tradisjonell arbeidsbelysning sparer man inntil 60% energi, levetid over 30.000 timer. RoHS og CE merket. Hvitt lys.<br  /><br  />


                </p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">6001050</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 1 280.00</span></h4>
               <h4 class="spec">-</h4>
            </div>

            <!--
          <div class="entryhalf">
          	<h2>NordLED oppladbar 10W</h2>
         	  <p class="productinfo">
              <img src="../images/produkter/3001510.jpg" class="left" style="border: 0;" />
              Oppladbart arbeidslys i aluminium og herdet glass. IP65. 100-240V. Ladetid 5 timer gir 4 timers brukstid. Leveres med lader 220V og 12V sigarettuttak.
			</p>
              <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
             <h4 class="pricesmall"><span class="art">5001010</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 560.00</span></h4>
             <h4 class="spec">&nbsp;</h4>
          </div>
          -->


          <!--
          <div class="entryhalf">
          	<h2>NordLED oppladbar 20W</h2>
          	  <p class="productinfo">
              <img src="../images/produkter/3001510.jpg" class="left" style="border: 0;" />
              Oppladbart arbeidslys i aluminium og herdet glass. IP65. 100-240V. Ladetid 5 timer gir 4 timers brukstid. Leveres med lader 220V og 12V sigarettuttak.
          	</p>
              <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
             <h4 class="pricesmall"><span class="art">5001020</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 1 020.00</span></h4>
             <h4 class="spec">&nbsp;</h4>
          </div>
          -->

          <!--
          <div class="entryhalf">
          	<h2>NordLED oppladbar 30W</h2>
          	  <p class="productinfo">
              <img src="../images/produkter/3001510.jpg" class="left" style="border: 0;" />
              Oppladbart arbeidslys i aluminium og herdet glass. IP65. 100-240V. Ladetid 5 timer gir 4 timers brukstid. Leveres med lader 220V og 12V sigarettuttak.
          	</p>
              <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
             <h4 class="pricesmall"><span class="art">5001030</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 1 140.00</span></h4>
             <h4 class="spec">&nbsp;</h4>
          </div>
          -->





           <!--<div class="entryhalf">
            	<h2>NordLED arbeidslys 10W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/3001210.jpg" class="left" style="border: 0;" />
                Arbeidslys 10W i aluminiumshus og herdet glass, værbestandig. IP65. Sammenlignet med tradisjonell arbeidsbelysning sparer man inntil 60% energi, levetid over 30.000 timer. RoHS og CE merket. Hvitt lys.<br  /><br  />
                Forbruk 0.11Amp, 100-240V.
                <br />
				720lm, 100&deg; spredning.
                <br /><br />


                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">6001010</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 520.00</span></h4>
               <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys_10w.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
            </div>-->

            

            <!--
            <div class="entryhalf">
            	<h2>NordLED arbeidslys 70W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/4001220.jpg" class="left" style="border: 0;" />
                Arbeidslys 70W i aluminiumshus og herdet glass, værbestandig. IP65. Sammenlignet med tradisjonell arbeidsbelysning sparer man inntil 60% energi, levetid over 30.000 timer. RoHS og CE merket. Hvitt lys.<br  /><br  />
                Forbruk 0.8Amp, 100-240V.
                <br />
				6 940lm, 100&deg; spredning.
                <br /><br />


                </p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">6001070</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 1 600.00</span></h4>
               <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys70w.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
            </div>




            -->
            <div class="entryhalf">
            	<h2>NordLED arbeidslys 100W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/6001100.jpg" class="left" style="border: 0;" />
                Arbeidslys 100W i aluminiumshus og herdet glass, værbestandig. IP65. Sammenlignet med tradisjonell arbeidsbelysning sparer man inntil 60% energi, levetid over 30.000 timer. RoHS og CE merket. Hvitt lys.<br  /><br  />
                Forbruk 1.2Amp, 100-240V.
                <br />
				8 690lm, 120&deg; spredning.
                <br /><br />


                </p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">6001100</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 2 800.00</span></h4>
               <h4 class="spec">-</h4>
            </div>

            <!--
            <div class="entryhalf">
            	<h2>NordLED arbeidslys 120W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/3001-120.jpg" class="left" style="border: 0;" />
                Arbeidslys 120W i aluminiumshus og herdet glass, værbestandig. IP65. Sammenlignet med tradisjonell arbeidsbelysning sparer man inntil 60% energi, levetid over 30.000 timer. RoHS og CE merket. Hvitt lys.<br  /><br  />
                Forbruk 1.5Amp, 100-240V.
                <br />
				11 480lm, 100&deg; spredning.
                <br /><br />


                </p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">6001120</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 2 800.00</span></h4>
               <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys120w.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
            </div>
            -->



            <!--
            <div class="entryhalf">
            	<h2>NordLED arbeidslys 150W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/3001-150.jpg" class="left" style="border: 0;" />
                Arbeidslys 150W i aluminiumshus og herdet glass, værbestandig. IP65. Sammenlignet med tradisjonell arbeidsbelysning sparer man inntil 60% energi, levetid over 30.000 timer. RoHS og CE merket. Hvitt lys.<br  /><br  />
                Forbruk 1.8Amp, 100-240V.
                <br />
				15 960lm, 100&deg; spredning.
                <br /><br />


                </p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">6001150</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 3 200.00</span></h4>
               <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys150w.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
            </div>



            <div class="entryhalf">
            	<h2>NordLED industrilampe 100W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/3001400.jpg" class="left" style="border: 0;" />
                Industrilampe med lang levetid, typisk brukt i fabrikker og lagerlokaler. IP65. Vann- og støvtett. Lampehus i aluminium, skjerm i glass.<br  /><br  />
                Forbruk 1.2Amp, 100-240V.
                <br />
				7 500lm, standard 45&deg; spredning. Kan leveres med 90&deg; eller 120&deg; spredning.
                <br /><br />


                </p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">7001100</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 2 200.00</span></h4>

            </div>

             <div class="entryhalf">
            	<h2>NordLED industrilampe 120W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/3001400.jpg" class="left" style="border: 0;" />
                Industrilampe med lang levetid, typisk brukt i fabrikker og lagerlokaler. IP65. Vann- og støvtett. Lampehus i aluminium, skjerm i glass.<br  /><br  />
                Forbruk 1.4Amp, 100-240V.
                <br />
				9 000lm, standard 45&deg; spredning. Kan leveres med 90&deg; eller 120&deg; spredning.
                <br /><br />


                </p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">7001120</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 2 600.00</span></h4>

            </div>

            <!-- <div class="entryhalf">
            	<h2>NordLED industrilampe 150W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/3001400.jpg" class="left" style="border: 0;" />
                Industrilampe med lang levetid, typisk brukt i fabrikker og lagerlokaler. IP65. Vann- og støvtett. Lampehus i aluminium, skjerm i glass.<br  /><br  />
                Forbruk 1.8Amp, 100-240V.
                <br />
				9 700lm, standard 45&deg; spredning. Kan leveres med 90&deg; eller 120&deg; spredning.
                <br /><br />


                </p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">3001450</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 2 900.00</span></h4>

            </div>
-->







        </div><!-- content end-->
	</div>
</div>

<?php require("../include/footer.php");?>


<script type="text/javascript"> Cufon.now(); </script>

</body>
</html>
