<?php
$page = "bilrekvisita";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LED-shop Norge</title>
<link href="../css/common.css" rel="stylesheet" type="text/css" />
<?php require("../include/javascripts.php");?>
</head>
<body>
<div id="wrap">

    <div id="page">
    	<?php require("../include/top.php");?>

        <div id="content">
        	<h1>Bilrekvisita</h1>
            <h2>alle priser er inklusiv merverdiavgift</h2>



            <div class="entryhalf">
              	<h2>NordLED  390</h2>
                	<p class="productinfo">
                  <img src="../images/produkter/7001050.jpg" class="left" style="border: 0;" />
                  Vanntette tilhengerlamper i sett, 12/24V, ECE-godkjent. Mål: 390x140x20mm.
                  </p>
                  <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                  <h4 class="pricesmall"><span class="art">7001042</span><span class="color">-</span><span class="volt">12/24V</span></h4>
                  <h4 class="spec">-</h4><h4 class="price small">pris per sett<br>kr. 1 559.00</h4>

              </div>
              <!-- entry end -->



          <!--
          <div class="entryhalf">
            	<h2>NordLED H1 12V</h2>
              	<p class="productinfo">
                <img src="../images/produkter/nordledh1.jpg" class="left" style="border: 0;" />
                Erstatter vanlig H1. 7.5W, 400lm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">-</span><span class="color">-</span><span class="volt">12V</span></h4>
                <h4 class="spec">-</h4><h4 class="price small">kr. 150.00</h4>

            </div>
            <!-- entry end -->

						<!--
            <div class="entryhalf">
            	<h2>NordLED H3 12V</h2>
              	<p class="productinfo">
                <img src="../images/produkter/nordledh3.jpg" class="left" style="border: 0;" />
                Erstatter vanlig H3. 7.5W, 400lm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">-</span><span class="color">-</span><span class="volt">12V</span></h4>
                <h4 class="spec">-</h4><h4 class="price small">kr. 150.00</h4>

            </div>
            <!-- entry end -->


						<!--
            <div class="entryhalf">
            	<h2>NordLED H4 12V</h2>
              	<p class="productinfo">
                <img src="../images/produkter/nordledh4.jpg" class="left" style="border: 0;" />
                Erstatter vanlig H4. 10W, 900lm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">-</span><span class="color">-</span><span class="volt">12V</span></h4>
                <h4 class="spec">-</h4><h4 class="price small">kr. 160.00</h4>

            </div>
            <!-- entry end -->

						<!--
            <div class="entryhalf">
            	<h2>NordLED H7 12V</h2>
              	<p class="productinfo">
                <img src="../images/produkter/nordledh7.jpg" class="left" style="border: 0;" />
                Erstatter vanlig H7. 10W, 900lm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">-</span><span class="color">-</span><span class="volt">12V</span></h4>
                <h4 class="spec">-</h4><h4 class="price small">kr. 160.00</h4>

            </div>
            <!-- entry end -->

						<!--
            <div class="entryhalf">
            	<h2>NordLED H8 12V</h2>
              	<p class="productinfo">
                <img src="../images/produkter/nordledh8.jpg" class="left" style="border: 0;" />
                Erstatter vanlig H8. 11W, 880lm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">-</span><span class="color">-</span><span class="volt">12V</span></h4>
                <h4 class="spec">-</h4><h4 class="price small">kr. 200.00</h4>

            </div>
            <!-- entry end -->

						<!--
          <div class="entryhalf">
            	<h2>NordLED H11 10-30V</h2>
              	<p class="productinfo">
                <img src="../images/produkter/nordledh8.jpg" class="left" style="border: 0;" />
                Erstatter vanlig H11. 10W, 900lm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">-</span><span class="color">-</span><span class="volt">10-30V</span></h4>
                <h4 class="spec">-</h4><h4 class="price small">kr. 180.00</h4>

            </div>
            <!-- entry end -->






          <div class="entryhalf">
            	<h2>NordLED  250</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001042.jpg" class="left" style="border: 0;" />
                Komplett baklampe 12-24V, 40 cm. kabel. Byggemål 250x80x24mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">7001042</span><span class="color">-</span><span class="volt">12V-24V</span></h4>
                <h4 class="spec">-</h4><h4 class="price small">kr. 680.00</h4>

            </div>
            <!-- entry end -->

            <div class="entryhalf">
            	<h2>NordLED  MaxiRund</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-091.jpg" class="left" style="border: 0;" />
                Komplett baklampe 12/24V. Fullstendig vanntett og rustfri.
                <br />Byggemål: 125Øx40mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">7001045</span><span class="color">-</span><span class="volt">12V/24V</span></h4>
                <h4 class="spec">-</h4><h4 class="price">kr. 480.00</h4>
            </div>
            <!-- entry end -->

            <!--
             <div class="entryhalf">
            	<h2>NordLED  Euro7</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-090.jpg" class="left" style="border: 0;" />
                Komplett baklampe 12/24V, ADR-godkjent. Fullstendig vanntett og rustfri. Byggemål: 392x152x44mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">7001040</span><span class="color">-</span><span class="volt">12V/24V</span></h4>
                <h4 class="spec">-</h4><h4 class="price small">pris per sett kr. 2 280.00</h4>

            </div>
            -->
            <!-- entry end -->



						<!--
            <div class="entryhalf">
            	<h2>NordLED Maxi</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-092.jpg" class="left" style="border: 0;" />
                Komplett baklampe 12/24V. ADR-godkjent. Fullstendig vanntett og rustfri. Byggemål: 370x135x40mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001050</span><span class="color">-</span><span class="volt">12V/24V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_maxi_3001050.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">pris per sett kr. 2 100.00</h4>
            </div>
            <!-- entry end -->
						<!--
            <div class="entryhalf">
            	<h2>NordLED 100</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-100.jpg" class="left" style="border: 0;" />
                Komplett baklampe 12/24V, ADR-godkjent. Fullstendig vanntett og rustfri. Slank utførsel, byggemål: 100x100x22mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001055</span><span class="color">-</span><span class="volt">12V/24V</span></h4>
                <h4 class="spec">-</h4><h4 class="price">kr. 400.00</h4>
            </div>
            <!-- entry end -->

            <div class="entryhalf">
            	<h2>NordLED 80</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-101.jpg" class="left" style="border: 0;" />
                Komplett baklampe 12/24V, ADR-godkjent. Fullstendig vanntett og rustfri. Slank utførsel, byggemål: 187x100x28mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">7001060</span><span class="color">-</span><span class="volt">12V/24V</span></h4>
                <h4 class="spec">-</h4><h4 class="price">kr. 540.00</h4>
            </div>
            <!-- entry end -->
						<!--
            <div class="entryhalf">
            	<h2>NordLED 150</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-105.jpg" class="left" style="border: 0;" />
                Komplett Baklampe 12/24V, ADR-godkjent. Kan leveres med 10m kabel. Fullstendig vanntett og rustfri. Slank utførsel, byggemål 150x80x24mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001065</span><span class="color">-</span><span class="volt">12V/24V</span></h4>
                <h4 class="spec">-</h4><h4 class="price">kr. 400.00</h4>
            </div>
            <!-- entry end -->

            <div class="entryhalf">
            	<h2>NordLED for båthenger</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-115.jpg" class="left" style="border: 0;" />
               	Komplett Baklampe 12/24V, ADR-godkjent med skiltlys (høyre). Fullstendig vanntett og rustfri. Byggemål høyre: 200x113x28mm, venstre: 200x92x28mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">7001070</span><span class="color">-</span><span class="volt">12V/24V</span></h4>
                <h4 class="spec">-</h4><h4 class="price">pris per sett kr. 900.00</h4>
            </div>
            <!-- entry end -->





            <div class="entryhalf">
            	<h2>NordLED markeringslys lavprofil</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-141.jpg" class="left" style="border: 0;" />
                Leveres i hvitt, rødt og oransje. 12/24V. Byggemål: 100x44x12mm. Leveres med 500mm kabel.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">7001010</span><span class="color">hvit</span><span class="volt">12/24V</span></h4>
                <h4 class="pricesmall"><span class="art">7001011</span><span class="color">oransje</span><span class="volt">12/24V</span></h4>
                <h4 class="pricesmall"><span class="art">7001012</span><span class="color">rød</span><span class="volt">12/24V</span></h4>
                <h4 class="spec">-</h4><h4 class="price">kr. 99.00</h4>
            </div>
            <!-- entry end -->

            <div class="entryhalf">
            	<h2>NordLED markeringslys rund</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-142.jpg" class="left" style="border: 0;" />
                Leveres i hvitt, rødt og oransje. 12/24V. Diameter lampe: 28mm, diameter hull: 25mm. Fullstendig vanntett og rustfri.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">7001015</span><span class="color">hvit</span><span class="volt">12/24V</span></h4>
                <h4 class="pricesmall"><span class="art">7001016</span><span class="color">oransje</span><span class="volt">12/24V</span></h4>
                <h4 class="pricesmall"><span class="art">7001017</span><span class="color">rød</span><span class="volt">12/24V</span></h4>
                <h4 class="spec">-</h4><h4 class="price">kr. 88.00</h4>
            </div>
            <!-- entry end -->
            <!-- fjernet 30.10.12
            <div class="entryhalf">
            	<h2>NordLED markeringslys</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-080.jpg" class="left" style="border: 0;" />
                Leveres i hvitt, rødt og oransje.<br />&nbsp;
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001020</span><span class="color">hvit</span><span class="volt">12/24V</span></h4>
                <h4 class="pricesmall"><span class="art">3001021</span><span class="color">oransje</span><span class="volt">12/24V</span></h4>
                <h4 class="pricesmall"><span class="art">3001022</span><span class="color">rød</span><span class="volt">12/24V</span></h4>
                <h4 class="spec">-</h4><h4 class="price">kr. 80.00</h4>
            </div>
            -->
            <!-- entry end -->

						<!--
            <div class="entryhalf">
            	<h2>NordLED LCD ryggekamera 5"</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-082.jpg" class="left" style="border: 0;" />
                Sett med vidvinkel-ryggekamera og 5"-LCD skjerm, fjernkontroll og kabel på 10m. Kamera har 18 infrarøde LED og fungerer godt i dårlig lys, automatisk dimming ved dag/natt. Skjermen har inngang for 3 enheter. Skjermmeny med 11 språk, baklys i knapper, elektroniske avstandsmarkører og innebygd høyttaler. Leveres med universalfeste.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="volt header">Info</span><span class="color header">Pris</span></h4>
                <h4 class="pricesmall"><span class="art">3001035</span><span class="volt">12/24V</span><span class="color"><strong>kr. 3 299.00</strong></span></h4>
                <h4 class="pricesmall"><span class="art">3001037</span><span class="volt">Ekstra IR-kamera</span><span class="color"><strong>kr. 880.00</strong></span></h4>
                <h4 class="pricesmall"><span class="art">3001038</span><span class="volt">Ekstra 5m. kabel</span><span class="color"><strong>kr. 330.00</strong></span></h4>
                <h4 class="pricesmall"><span class="art">3001039</span><span class="volt">Ekstra 10m. kabel</span><span class="color"><strong>kr. 430.00</strong></span></h4>
                <h4 class="spec">-</h4><h4 class="price">-</h4>
          </div>
            <!-- entry end -->
						<!--
            <div class="entryhalf">
            	<h2>NordLED LCD ryggekamera 7"</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-083.jpg" class="left" style="border: 0;" />
                Sett med vidvinkel-ryggekamera og 7"-LCD skjerm, fjernkontroll og kabel på 10m. Kamera har 18 infrarøde LED og fungerer godt i dårlig lys, automatisk dimming ved dag/natt. Skjermen har inngang for 3 enheter. Skjermmeny med 11 språk, baklys i knapper, elektroniske avstandsmarkører og innebygd høyttaler. Leveres med universalfeste.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="volt header">Info</span><span class="color header">Pris</span></h4>
                <h4 class="pricesmall"><span class="art">3001036</span><span class="volt">12/24V</span><span class="color"><strong>kr. 3 899.00</strong></span></h4>
                <h4 class="pricesmall"><span class="art">3001037</span><span class="volt">Ekstra IR-kamera</span><span class="color"><strong>kr. 880.00</strong></span></h4>
                <h4 class="pricesmall"><span class="art">3001038</span><span class="volt">Ekstra 5m. kabel</span><span class="color"><strong>kr. 330.00</strong></span></h4>
                <h4 class="pricesmall"><span class="art">3001039</span><span class="volt">Ekstra 10m. kabel</span><span class="color"><strong>kr. 430.00</strong></span></h4>
                <h4 class="spec">-</h4><h4 class="price">-</h4>
            </div>
            <!-- entry end -->







            <div class="entryhalf">
            	<h2>NordLED skiltbelysning 20</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001030.jpg" class="left" style="border: 0;" />
                Skiltbelysning for 12/24V. <br />
                Mål: 30x65x24mm


                </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">7001030</span><span class="color">-</span><span class="volt">12/24V</span></h4>
                <h4 class="spec">-</h4><h4 class="price">kr. 100.00</h4>
            </div>
            <!-- entry end -->

            <!--
            <div class="entryhalf">
            	<h2>NordLED skiltbelysning 40</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001032.jpg" class="left" style="border: 0;" />
                Skiltbelysning for 12/24V. ECE-godkjent. 23cm kabel.<br />
				Mål: 50x72x41mm<br />

                </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">7001032</span><span class="color">-</span><span class="volt">12/24V</span></h4>
                <h4 class="spec">-</h4><h4 class="price">kr. 120.00</h4>
            </div>
            -->
            <!-- entry end -->



            <!--
            <div class="entry">
            	<h2>NordLED kjørelys</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-078.jpg" class="left" style="border: 0;" />
                ECE E9 og ADR-godkjent, vanntett kjørelys i pulverlakkert aluminium, med vibrasjons- og støtdyktig deksel. Leveres i sett. LBH: 208x74x23mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001025</span><span class="color">-</span><span class="volt">9V-36V</span></h4>
                <h4 class="spec">-</h4><h4 class="pricewide">kr. 1 200.00</h4>
            </div>-->
            <!-- entry end -->


            <div class="entry">
            	<h2>NordLED Pilot</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001100.jpg" class="left" style="border: 0;" />
                Små lamper til styling av biler, utvendig og innvendig. Rustfritt stål i lampene, 12mm hull for montering.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">7001100</span><span class="color">rød</span><span class="volt">12V</span></h4>
                <h4 class="pricesmall"><span class="art">7001101</span><span class="color">blå</span><span class="volt">12V</span></h4>
                <h4 class="pricesmall"><span class="art">7001102</span><span class="color">hvit</span><span class="volt">12V</span></h4>
                <h4 class="pricesmall"><span class="art">7001103</span><span class="color">oransje</span><span class="volt">12V</span></h4>
                <h4 class="pricesmall"><span class="art">7001104</span><span class="color">grønn</span><span class="volt">12V</span></h4>
                <h4 class="pricesmall"><span class="art">7001105</span><span class="color">rød</span><span class="volt">24V</span></h4>
                <h4 class="pricesmall"><span class="art">7001106</span><span class="color">blå</span><span class="volt">24V</span></h4>
                <h4 class="pricesmall"><span class="art">7001107</span><span class="color">hvit</span><span class="volt">24V</span></h4>
                <h4 class="pricesmall"><span class="art">7001108</span><span class="color">oransje</span><span class="volt">24V</span></h4>
                <h4 class="pricesmall"><span class="art">7001109</span><span class="color">grønn</span><span class="volt">24V</span></h4>
                <h4 class="spec">-</h4><h4 class="pricewide">pris pr stk kr. 60.00</h4>
            </div>
            <!-- entry end -->







              <div class="entryhalf">
              	<h2>NordLED tilhengerkontakt</h2>
                	<p class="productinfo">
                  <img src="../images/produkter/tilhenger.jpg" class="left" style="border: 0;" />
                  Tilhengerkontakt 7-stikk med 1m. kabel, skjøtekabel for kontakt & lampe-til-lampe kabler.
                  </p>
                  <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="volt header">Artikkel</span><span class="color header">Pris</span></h4>
                  <h4 class="pricesmall"><span class="art">7001220</span><span class="volt">Kontakt 7-stikk med 1m. kabel</span><span class="color"><strong>kr. 150.00</strong></span></h4>
                  <h4 class="pricesmall"><span class="art">7001221</span><span class="volt">Kabel 3.7m</span><span class="color"><strong>kr. 115.00</strong></span></h4>
                  <h4 class="pricesmall"><span class="art">7001222</span><span class="volt">Kabel 4.3m</span><span class="color"><strong>kr. 120.00</strong></span></h4>
                  <h4 class="pricesmall"><span class="art">7001223</span><span class="volt">Kabel 4.9m</span><span class="color"><strong>kr. 125.00</strong></span></h4>
                  <h4 class="pricesmall"><span class="art">7001224</span><span class="volt">Kabel 5.5m</span><span class="color"><strong>kr. 130.00</strong></span></h4>
                  <h4 class="pricesmall"><span class="art">7001225</span><span class="volt">Kabel 6.1m</span><span class="color"><strong>kr. 135.00</strong></span></h4>
                  <h4 class="pricesmall"><span class="art">7001226</span><span class="volt">Kabel 6.7m</span><span class="color"><strong>kr. 140.00</strong></span></h4>
                  <h4 class="pricesmall"><span class="art">7001227</span><span class="volt">Kabel 7.3m</span><span class="color"><strong>kr. 145.00</strong></span></h4>
                  <h4 class="pricesmall"><span class="art">7001228</span><span class="volt">Kabel lampe-til-lampe 1.1m</span><span class="color"><strong>kr. 60.00</strong></span></h4>
                  <h4 class="pricesmall"><span class="art">7001229</span><span class="volt">Kabel lampe-til-lampe 1.3m</span><span class="color"><strong>kr. 70.00</strong></span></h4>
                  <h4 class="pricesmall"><span class="art">7001230</span><span class="volt">Kabel lampe-til-lampe 1.5m</span><span class="color"><strong>kr. 80.00</strong></span></h4>
                  <h4 class="pricesmall"><span class="art">7001231</span><span class="volt">Kabel lampe-til-lampe 1.7m</span><span class="color"><strong>kr. 90.00</strong></span></h4>
                  <h4 class="pricesmall"><span class="art">7001232</span><span class="volt">Kabel lampe-til-lampe 1.9m</span><span class="color"><strong>kr. 100.00</strong></span></h4>
                  <h4 class="pricesmall"><span class="art">7001233</span><span class="volt">Kabel lampe-til-lampe 2.1m</span><span class="color"><strong>kr. 110.00</strong></span></h4>
                  <h4 class="spec">-</h4><h4 class="price">-</h4>
              </div>
              <!-- entry end -->


            <div class="entryhalf">
              	<h2>Tilhengerkontakt oversikt</h2>
                	<p class="productinfo">
                  <img src="../images/produkter/tilhengerkontaktill.jpg" class="left" style="border: 0; margin-bottom: 100px;"/>

              </div>
              <!-- entry end -->








            <div class="entryhalf">
              	<h2>NordLED CAN-bus ledningssett</h2>
                	<p class="productinfo">
                  <img src="../images/produkter/canbus.jpg" class="left" style="border: 0;" />
                  <strong>Universal ledningssett for Can Bus tilkobling av LED til europeiske biler. </strong>
                  <br /><br />
                  Settet monteres i biler for å forhindre feilmelding ved bruk av LED-dioder og/eller lamper ved ettermontering.
                  </p>
                   <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                  <h4 class="pricesmall"><span class="art">7001090</span><span class="color">-</span><span class="volt">-</span></h4>
                  <h4 class="spec">-</h4><h4 class="price">kr. 720.00</h4>
              </div>
              <!-- entry end -->




            <div class="entryhalf">
            	<h2>NordLED effektmotstand</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-088.jpg" class="left" style="border: 0;" />
                <img src="../images/produkter/2004-088-2.jpg" class="left" style="border: 0;" />
                Når man monterer lamper med lysdioder, kan noen biler som har CAN-bus overvåkning/styring gi en feilmelding. En måte å motvirke dette på er å montere en effektmotstand for hver lampefunksjon. 12V eller 24V, 50W 6.0 Ohm. Byggemål: 50x15x15mm.
                <br /><br />
                Komponenten må monteres på strømkretsen før selve LED-lampen.
                <br /><br />
                Må monteres på metalloverflate da komponenten kan bli svært varm (opp til 170&deg;C). Skal ikke monteres på plast, slanger eller malte overflater. Benytt vedlagte kabelsko for å sikre vanntett kobling.
                </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">7001085</span><span class="color">-</span><span class="volt">12V</span></h4>
                <h4 class="pricesmall"><span class="art">7001086</span><span class="color">-</span><span class="volt">24V</span></h4>
                <h4 class="spec">-</h4><h4 class="price">kr. 120.00</h4>
            </div>
            <!-- entry end -->





            <!--
            <div class="entryhalf">
            	<h2>NordLED pos. markeringslys</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-073.jpg" class="left" style="border: 0;" />
                Leveres med rødt og hvitt LED-lys.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001075</span><span class="color">-</span><span class="volt">12V/24V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_posmarkeringslys_3001075.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 240.00</h4>
            </div>
            <!-- entry end -->

            <!--
            <div class="entryhalf">
            	<h2>NordLED pos. markeringslys</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-074.jpg" class="left" style="border: 0;" />
                Leveres i sett av 2 med rødt og hvitt LED-lys.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001080</span><span class="color">rød/hvit</span><span class="volt">12V/24V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_posmarkeringslys_3001080.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 580.00</h4>
            </div>
            <!-- entry end -->

            <!-- fjernet 30.10.12
            <div class="entryhalf">
            	<h2>NordLED leselys </h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-153.jpg" class="left" style="border: 0;" />
                Små interiørlamper 12V i flere farger. ØxH: 75x17mm. Både fargede og hvite led i samme lampe.
                Blått leselys for dag og rødt leselys for natt. Husk å oppgi farge ved bestilling
                <br /><br />
                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">1001040</span><span class="color">blå</span><span class="volt">12V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001041</span><span class="color">rød</span><span class="volt">12V</span></h4>
                 <h4 class="spec">-</h4><h4 class="price">kr. 229.00</h4>
            </div>
            -->
            <!-- entry end -->

            <!-- fjernet 30.10.12
            <div class="entryhalf">
            	<h2>NordLED ledelys/markørlys </h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-152.jpg" class="left" style="border: 0;" />
                Små interiørlamper 12V i flere farger. Størrelse: 100x25mm. Hardplast deksel. 10cm kabel.
                Perfekt for hytter, båter, campingvogner og biler.
                Husk å oppgi farge ved bestilling.
                <br /><br /><br />

                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">1001045</span><span class="color">grønn</span><span class="volt">12V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001046</span><span class="color">rød</span><span class="volt">12V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001047</span><span class="color">blå</span><span class="volt">12V</span></h4>
                 <h4 class="spec">-</h4><h4 class="price">kr. 179.00</h4>
            </div>
            -->
            <!-- entry end -->

            <!-- fjernet 03.11.12
            <div class="entryhalf">
            	<h2>NordLED interiørbelysning</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-150.jpg" class="left" style="border: 0;" />
                Små interiørlamper i 12V eller 24V i flere farger. Størrelse: 65x30mm. Forkrommet metall i lampefeste og hardplast deksel. 14cm kabel.
                Perfekt for hytter, båter, campingvogner og biler.
                Husk å oppgi farge ved bestilling.
                <br /><br />
                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">1001050</span><span class="color">blå</span><span class="volt">12V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001051</span><span class="color">rød</span><span class="volt">12V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001052</span><span class="color">grønn</span><span class="volt">12V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001053</span><span class="color">hvit</span><span class="volt">12V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001055</span><span class="color">blå</span><span class="volt">24V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001056</span><span class="color">rød</span><span class="volt">24V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001057</span><span class="color">grønn</span><span class="volt">24V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001058</span><span class="color">hvit</span><span class="volt">24V</span></h4>
                 <h4 class="spec">-</h4><h4 class="price">kr. 179.00</h4>
            </div>
            <!-- -->
            <!-- entry end -->

            <!-- fjernet 03.11.12
            <div class="entryhalf">
            	<h2>NordLED interiørbelysning</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-120.jpg" class="left" style="border: 0;" />
                Disse små LED-lamper tilsvarer dagens glødepærer på 15W. Leveres i krom, gull og hvitt. ØxH: 75x17mm. Leveres i 12V eller 24V.
                Perfekt for hytter, båter, campingvogner og biler.
                Husk å oppgi farge ved bestilling.
                <br /><br /><br /><br />

                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">1001061</span><span class="color">krom</span><span class="volt">12V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001062</span><span class="color">gull</span><span class="volt">12V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001063</span><span class="color">hvit</span><span class="volt">12V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001065</span><span class="color">krom</span><span class="volt">24V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001066</span><span class="color">gull</span><span class="volt">24V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001067</span><span class="color">hvit</span><span class="volt">24V</span></h4>
                 <h4 class="spec">-</h4><h4 class="price">kr. 239.00</h4>
            </div>
            <!-- ->
            <!-- entry end -->


            <div class="entryhalf">
            	<h2>NordLED lyslist</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-124.jpg" class="left" style="border: 0;" />
                Disse små lyssterke LED-lyslister passer til hus, hytter, biler og båter. Slank utførelse, kun 10mm høy og 25mm bred. Leveres i lengder 150mm, 300mm og 600mm. Leveres kun i hvitt med 12V eller 24V.
                Lysstyrken på 150mm tilsvarer 15W glødepære, 300mm tilsvarer 30W glødepære og 600mm tilsvarer 60W glødepære.
                <br /><br />
                 </p>
                  <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header">Pris</span></h4>
                  <h4 class="pricesmall"><span class="art">1001070</span><span class="color">150mm 12V</span><span class="volt">kr. 480.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001071</span><span class="color">300mm 12V</span><span class="volt">kr. 419.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001072</span><span class="color">600mm 12V</span><span class="volt">kr. 619.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001075</span><span class="color">150mm 24V</span><span class="volt">kr. 680.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001076</span><span class="color">300mm 24V</span><span class="volt">kr. 480.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001077</span><span class="color">600mm 24V</span><span class="volt">kr. 680.00</span></h4>
                  <h4 class="spec"><a href="../produktspesifikasjon/nordled_lyslist_1001070.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">-</h4>
            </div>
            <!-- entry end -->


            <div class="entryhalf">
            	<h2>NordLED TAXI-lys 300</h2>
              	<p class="productinfo">
                <img src="../images/produkter/1001071.jpg" class="left" style="border: 0;" />
                Denne 300mm lyslisten passer perfekt til taxiskilt, slank utførsel, kun 10mm høy og 25mm bred. Minimalt strømforbruk: 0.31A ved 12V og 0.17A ved 24V.
                <br /><br />
                 </p>
                  <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header">Volt</span></h4>
                  <h4 class="pricesmall"><span class="art">1001071</span><span class="color">300mm</span><span class="volt">12V</span></h4>
                  <h4 class="pricesmall"><span class="art">1001076</span><span class="color">300mm</span><span class="volt">24V</span></h4>
                  <h4 class="spec">-</h4><h4 class="price">kr. 419.00</h4>
            </div>
            <!-- entry end -->







        </div><!-- content end-->
	</div>
</div>

<?php require("../include/footer.php");?>


<script type="text/javascript"> Cufon.now(); </script>

</body>
</html>
