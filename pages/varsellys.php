<?php
$page = "varsellys";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LED-shop Norge</title>
<link href="../css/common.css" rel="stylesheet" type="text/css"/>
<?php require("../include/javascripts.php");?>
</head>
<body>
<div id="wrap">

    <div id="page">
    	<?php require("../include/top.php");?>

        <div id="content">
        	<h1>Varsellys</h1>
            <h2>alle priser er inklusiv merverdiavgift</h2>

            <!--
           <div class="entryhalf">
            	<h2>NordLED varsellys Q4</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2001080.jpg" class="left" style="border: 0;" />
                Varsellys med 4x1W LED med 18 blinkmønstre. Forbruk 0.27A @12.8VDC / 0.13A @24VDC avhengig av blinkmønster. Små lamper som måler ca 72x72 mm og 28mm tykkelse.</p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="volt header">Farge / Info</span><span class="color header">Pris</span></h4>
                <h4 class="pricesmall"><span class="art">2001022</span><span class="volt">gul - 11-30V.</span><span class="color"><strong>kr. 940.00</strong></span></h4>
                <h4 class="pricesmall"><span class="art">2001023</span><span class="volt">blå - 11-30V.</span><span class="color"><strong>kr. 1 120.00</strong></span></h4>
             </div>
            <!-- entry end -->
            

           <div class="entryhalf">
            	<h2>NordLED 8-dioder</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2001-060.jpg" class="left" style="border: 0;" />
                Varsellys med 8 stk 3. generasjon LED. Kan koble opp til 10 stk i serie. 26 blinkmønstre. Dimensjon: 110x28x50mm. Sertifisering EMC. IP67.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="volt header">Farge / Info</span><span class="color header">Pris</span></h4>
                <h4 class="pricesmall"><span class="art">2001027</span><span class="volt">gul - 12/24V.</span><span class="color"><strong>kr. 1 890.00</strong></span></h4>
                <h4 class="pricesmall"><span class="art">2001028</span><span class="volt">blå - 12/24V. </span><span class="color"><strong>kr. 2 290.00</strong></span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/2001027.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
            </div>
            <!-- entry end -->

            <div class="entryhalf">
              	<h2>NordLED flat 6-dioder 12/24V</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2001016-2.jpg" class="left" style="border: 0;" />
                Varsellys med 19 blinkmønstre, lav profil for diskrét montering. 10W, 0.5A@12V / 0.25A@24V. Levetid opp til 100.000 timer. Vær- og vibrasjonsbestandig. Sertifisert for SAE J595 klasse1, ECE R10-04. Mål: 112x28x9mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="volt header">Farge / Info</span><span class="color header">Pris</span></h4>
                <h4 class="pricesmall"><span class="art">2001016</span><span class="volt">gul - 12/24V.</span><span class="color"><strong>kr. 1 590.00</strong></span></h4>
            </div>
            <!-- entry end -->

             <div class="entryhalf">
            	<h2>NordLED 3-LED 180&deg; varsellys</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2001051.jpg" class="left" style="border: 0;" />
                Varsellampe med 180&deg; synlighet, 12 blinkmønstre, 3x1W. Forbruk 0.01-0.15A@13.8V, 0.03-0.06A@28V. Sertifisering ECE EMC. IP67. Dimensjon: 111x37x23mm
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="volt header">Farge / Info</span><span class="color header">Pris</span></h4>
                <h4 class="pricesmall"><span class="art">2001051</span><span class="volt">gul - 12/24V.</span><span class="color"><strong>kr. 1 240.00</strong></span></h4>
                <h4 class="pricesmall"><span class="art">2001052</span><span class="volt">blå - 12/24V.</span><span class="color"><strong>kr. 1 440.00</strong></span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/2001051.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
            </div>
            <!-- entry end -->


            <div class="entryhalf">
            	<h2>NordLED D2-Basic 200</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2001050.jpg" class="left" style="border: 0;" />
                Praktisk varsellampe med enkelt blink, bruker 2 stk 6V 4R25-batterier. EN12352 sertifisert.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">2001050</span><span class="color">gul</span><span class="volt">6V 2 batterier</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_d2basic.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 1 040.00</h4>
            </div>
            <!-- entry end -->


            <div class="entryhalf">
            	<h2>NordLED D4-Basic 200</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2001055.jpg" class="left" style="border: 0;" />
                Praktisk varsellampe med enkelt blink, bruker 4 stk 6V 4R25-batterier. EN12352 sertifisert.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">2001055</span><span class="color">gul</span><span class="volt">6V 4 batterier</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_d4basic.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 1 240.00</h4>
            </div>
            <!-- entry end -->


            <div class="entry">
            	<h2>NordLED mini 36 varsellys 12/24V</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2001070.jpg" class="left" style="border: 0;" />
                3. generasjons multivolt lampe med 18 blinkmønstere, vær- og vibrasjonsbestandig.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">2001031</span><span class="color">gul</span><span class="volt">12/24V permanent</span></h4>
                <h4 class="pricesmall"><span class="art">2001032</span><span class="color">gul</span><span class="volt">12/24V magnetfeste</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_2001070.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="pricewide">kr. 4 400.00</h4>
            </div>
            <!-- entry end -->



            <!--
          	<div class="entry">
            	<h2>NordLED F1 varsellys 12-24V</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-050.jpg" class="left" style="border: 0;" />
                Et sett som består av 2 stk. varsellys, styreenhet og kabler. Disse kan monteres i lamper eller der man ønsker. 3W x 9 punkter pr lampe.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">2001001</span><span class="color">gul</span><span class="volt">12-24V, 19 blinkmønstre</span></h4>
                <h4 class="pricesmall"><span class="art">2001002</span><span class="color">hvit</span><span class="volt">12-24V, 19 blinkmønstre</span></h4>
                <h4 class="pricesmall"><span class="art">2001003</span><span class="color">blå</span><span class="volt">12-24V, 19 blinkmønstre</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_f1_2001001.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="pricewide">kr. 1 990.00</h4>
            </div>
            -->
            <!-- entry end -->


            <div class="entryhalf">
            	<h2>NordLED B14</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2001-004-2.jpg" class="left" style="border: 0;" />
                En liten, kompakt varsellampe med 10x3W LED. 11 blinkmønstre. 12/24V. Lampen kan synkroniseres med andre lamper i samme serie.
                ECE-R65, ECE-R10, SAE.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">2001004g</span><span class="color">gul</span><span class="volt">10-24V</span></h4>
                <h4 class="pricesmall"><span class="art">2001004r</span><span class="color">rød</span><span class="volt">10-24V</span></h4>
                <h4 class="pricesmall"><span class="art">2001004b</span><span class="color">blå</span><span class="volt">10-24V</span></h4>
                <h4 class="pricesmall"><span class="art">2001004h</span><span class="color">hvit</span><span class="volt">10-24V</span></h4>
                <h4 class="spec">-</h4><h4 class="price">kr. 1 980.00</h4>
            </div>
            <!-- entry end -->

            <!--
            <div class="entryhalf">
            	<h2>NordLED Surface-40</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2001008.jpg" class="left" style="border: 0;" />
                LED-modul som bygger kun 9mm og monteres svært enkelt rett på de fleste overflater. 40 LED-punkter per modul, 8 blinkmønstre. 12V 0.5Amp.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">2001008</span><span class="color">gul</span><span class="volt">-</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_2001008.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 695.00</h4>
            </div>
            -->
            <!-- entry end -->

            <!--
            <div class="entryhalf">
            	<h2>NordLED Surface-70</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2001009.png" class="left" style="border: 0;" />
                LED-modul som bygger kun 9mm og monteres svært enkelt rett på de fleste overflater. 56 LED-punkter per modul, 8 blinkmønstre. 12V 0.8Amp.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">2001009</span><span class="color">gul</span><span class="volt">-</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_2001009.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 840.00</h4>
            </div>
            -->
            <!-- entry end -->

            <!--
            <div class="entryhalf">
            	<h2>NordLED Surface-18</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2001007.jpg" class="left" style="border: 0;" />
                LED-modul som bygger kun 9mm og monteres svært enkelt rett på de fleste overflater. 18 LED-punkter per modul, 8 blinkmønstre. 12V 0.5Amp.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">2001007</span><span class="color">gul</span><span class="volt">-</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_2001007.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 780.00</h4>
            </div>
            -->
            <!-- entry end -->

            <div class="entryhalf">
            	<h2>NordLED mini 6-dioder 12-24V</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2001012.png" class="left" style="border: 0;" />
                Varsellys med 6-LED-punkter som monteres med medfølgende festebrakett. Lampehus i aluminium og slagfast polymerplast. Levetid ca. 100.000 timer.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">2001012</span><span class="color">gul</span><span class="volt">12-24V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_2001012.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 940.00</h4>
            </div>
            <!-- entry end -->



            <!--<div class="entryhalf">
            	<h2>NordLED F2</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2001014.png" class="left" style="border: 0;" />
                Varsellys med 6 LED-punkter, kun 22mmm høy, perfekt der det viktig med kraftig lys på liten plass. Leveres med 2.7 meter kabel, 25 blinkmønstre. 10-16V.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">2001014</span><span class="color">gul</span><span class="volt">10-16V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_2001014.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 750.00</h4>
            </div>
             entry end -->




            <div class="entryhalf">
            	<h2>NordLED 16 varsellys</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-033.jpg" class="left" style="border: 0;" />
                Liten hendig varsellampe med batteri og magnet som drives av 2 stk D-batterier.	300 timer batterilevetid. 16 LED-punkter.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">2001005</span><span class="color">gul</span><span class="volt">-</span></h4>
                <h4 class="spec">-</h4><h4 class="price">kr. 860.00</h4>
            </div>
            <!-- entry end -->


            <div class="entryhalf">
            	<h2>NordLED 24 varsellys</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-034.jpg" class="left" style="border: 0;" />
                Liten hendig varsellampe med batteri og magnet som drives av 2 stk D-batterier.
				400 timer batterilevetid. 24 LED-punkter.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">2001006</span><span class="color">gul</span><span class="volt">-</span></h4>
                <h4 class="spec">-</h4><h4 class="price">kr. 960.00</h4>
            </div>
            <!-- entry end -->


            <div class="entry">
            	<h2>NordLED lysbjelke 10-30V</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2001010-1.jpg" class="left" style="border: 0;" />
                Varsellysbjelke med 2 LED-moduler med 9x3W Led og 4 ulike blinkmønstre, som alle er ECE R65-godkjent. ECE R10-godkjent. Lampene leveres i 3 ulike lengder, 915mm, 1220mm og 1524mm.
10-30V. Gummiføtter som kan justeres i bredde, festes til kjøretøy med M8 bolter.Høyde 99mm, bredde 211mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Lengde</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">2001010</span><span class="color">915mm</span><span class="volt">kr. 4.990,-</span></h4>
                <h4 class="pricesmall"><span class="art">2001011</span><span class="color">1220mm</span><span class="volt">kr. 5.990,-</span></h4>
                <h4 class="pricesmall"><span class="art">2001012</span><span class="color">1524mm</span><span class="volt">kr. 7.490,-</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/2001010-1.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
            </div>
            <!-- entry end -->

            <!--
            <div class="entry">
            	<h2>NordLED 32 varsellys &amp; trafikkdirigent</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-030.jpg" class="left" style="border: 0;" />
                Effektivt og værbestandig varsellys med 32 høyintensitets LED. 22 ulike blinkmønstre, hvorav 11 er retningsbestemt. Lampen er ideel for å dirigere trafikk. Leveres med 2 lampeenheter for sammenkobling, 1 kontrollenhet og 7.6 meter kabel.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Feste</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">2001015</span><span class="color">permanent</span><span class="volt">12/24V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_32varsellys_2001015.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="pricewide">kr. 5 200.00</h4>
            </div>
            <!-- entry end -->


            <div class="entryhalf">
            	<h2>NordLED 3-dioder</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2001020.jpg" class="left" style="border: 0;" />
                Varsellys med 3x1W LED lysdioder. Dimensjon: 99x41x29mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">2001020</span><span class="color">gul</span><span class="volt">12/24V. 12 blinkmønstre.</span></h4>
                <h4 class="pricesmall"><span class="art">2001021</span><span class="color">blå</span><span class="volt">12/24V. 12 blinkmønstre.</span></h4>
                <h4 class="spec">&nbsp;</h4><h4 class="price">kr. 840.00</h4>
            </div>
            <!-- entry end -->

            <div class="entryhalf">
            	<h2>NordLED 4-dioder</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2001025.jpg" class="left" style="border: 0;" />
                Varsellys med 4x1W LED lysdioder. Dimensjon: 122x41x29mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">2001025</span><span class="color">gul</span><span class="volt">12/24V 12 blinkmønstre</span></h4>
                <h4 class="pricesmall"><span class="art">2001026</span><span class="color">blå</span><span class="volt">12/24V 12 blinkmønstre</span></h4>
               <h4 class="spec">&nbsp;</h4> <h4 class="price">kr. 960.00</h4>
            </div>
            <!-- entry end -->

            <!--
            <div class="entryhalf">
            	<h2>NordLED 4-dioder vindu</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-064.jpg" class="left" style="border: 0;" />
                Varsellys med 4 LED dioder. Leveres også med blått lys.
                <br />
                &nbsp;
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">2001030</span><span class="color">gul</span><span class="volt">4x1W 12V 19 blinkmønstre</span></h4>
                <h4 class="pricesmall"><span class="art">2001031</span><span class="color">blå</span><span class="volt">4x1W 12V 19 blinkmønstre</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_4dioder_2001030.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 980.00</h4>
            </div>
            <!-- entry end -->

            <!--
            <div class="entryhalf">
            	<h2>NordLED ML-50 varsellys</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2001013.jpg" class="left" style="border: 0;" />
                Varsellys med permanent feste, 24 LED, 11 blinkmønstre. 12-48V, forbruk: 0.8A@12V, 0.3A@48V.
                <br />
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="volt header">Farge / Info</span><span class="color header">Pris</span></h4>
                <h4 class="pricesmall"><span class="art">2001013</span><span class="volt">gul / lav (113mm)</span><span class="color"><strong>kr. 1 800.00</strong></span></h4>
                <h4 class="pricesmall"><span class="art">2001013</span><span class="volt">gul / høy (160mm)</span><span class="color"><strong>kr. 2 000.00</strong></span></h4>
                <h4 class="pricesmall"><span class="art">2001014</span><span class="volt">blå / lav (113mm)</span><span class="color"><strong>kr. 2 000.00</strong></span></h4>
                <h4 class="pricesmall"><span class="art">2001014</span><span class="volt">blå / høy (160mm)</span><span class="color"><strong>kr. 2 200.00</strong></span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/2001013.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
            </div>
            -->


            <!--
            <div class="entryhalf">
            	<h2>NordLED ML-55 varsellys</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-068.jpg" class="left" style="border: 0;" />
                Finnes i rød, gul, blå og klar. Vann- og vibrasjonssikker.
                <br />
                <strong>Gitter leveres som tilbehør kr. 125,- (artnr 2001036)</strong>
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">2001013</span><span class="color">gul</span><span class="volt">12-80V 4 blinkmønstre</span></h4>
                <h4 class="pricesmall"><span class="art">2001013g</span><span class="color">-</span><span class="volt">gitter</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_ml55_2001035.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 860.00</h4>
            </div>
             entry end -->



        </div><!-- content end-->
	</div>
</div>

<?php require("../include/footer.php");?>


<script type="text/javascript"> Cufon.now(); </script>

</body>
</html>
