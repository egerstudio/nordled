<?php
$page = "kontakt";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LED-shop Norge</title>
<link href="../css/common.css" rel="stylesheet" type="text/css" />
<?php require("../include/javascripts.php");?>
</head>
<body>
<div id="wrap">

    <div id="page">
    	<?php require("../include/top.php");?>

        <div id="content">
        	<h1>Kontakt oss</h1>
            <div class="entry">
	           <p style="font-size: 18px;">Send din bestilling til oss på mail: <a href="mailto:info@nordled.no">info@nordled.no</a><br />
<br />
Det er viktig at du inkluderer navn, adresse, telefon og tydelig informasjon om varenummer og antall.</p>



          	</div>


        </div><!-- content end-->
	</div>
</div>

<?php require("../include/footer.php");?>


<script type="text/javascript"> Cufon.now(); </script>

</body>
</html>
