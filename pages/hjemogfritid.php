<?php
$page = "hjemfritid";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LED-shop Norge</title>
<link href="../css/common.css" rel="stylesheet" type="text/css" />
<?php require("../include/javascripts.php");?>
</head>
<body>
<div id="wrap">

    <div id="page">
    	<?php require("../include/top.php");?>

        <div id="content">
        	<h1>Hjem & fritidsprodukter</h1>
            <h2>alle priser er inklusiv merverdiavgift</h2>




            <div class="entry">
              <h2>NordLED T5 lysrør</h2>
                <p class="productinfo">
                <img src="../images/produkter/t5t8.jpg" class="left" style="border: 0;" />
                Sømløse T5 komplette lysrør som kan kobles i serie opp til 30 meter. AC95-265V, levetid 30.000 timer. 140&deg; spredning.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
                  <h4 class="pricesmall"><span class="art">1001002</span><span class="color">600mm 8W hvit</span><span class="volt" style="text-align: right;">kr. 520.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001003</span><span class="color">600mm 8W nøytral</span><span class="volt" style="text-align: right;">kr. 520.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001004</span><span class="color">600mm 8W varmhvit</span><span class="volt" style="text-align: right;">kr. 520.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001005</span><span class="color">900mm 12W hvit</span><span class="volt" style="text-align: right;">kr. 560.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001006</span><span class="color">900mm 12W nøytral</span><span class="volt" style="text-align: right;">kr. 560.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001007</span><span class="color">900mm 12W varmhvit</span><span class="volt" style="text-align: right;">kr. 560.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001008</span><span class="color">1200mm 16W hvit</span><span class="volt" style="text-align: right;">kr. 620.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001009</span><span class="color">1200mm 16W nøytral</span><span class="volt" style="text-align: right;">kr. 620.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001010</span><span class="color">1200mm 16W varmhvit</span><span class="volt" style="text-align: right;">kr. 620.00</span></h4>
            </div>

            <div class="entry">
              <h2>NordLED T8 lysrør</h2>
                <p class="productinfo">
                <img src="../images/produkter/t5t8.jpg" class="left" style="border: 0;" />
                Sømløse T8 komplette lysrør som kan kobles i serie opp til 30 meter. AC95-265V, levetid 30.000 timer. 140&deg; spredning.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
                  <h4 class="pricesmall"><span class="art">1001015</span><span class="color">600mm 8W hvit</span><span class="volt" style="text-align: right;">kr. 520.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001016</span><span class="color">600mm 8W nøytral</span><span class="volt" style="text-align: right;">kr. 520.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001017</span><span class="color">600mm 8W varmhvit</span><span class="volt" style="text-align: right;">kr. 520.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001018</span><span class="color">1200mm 18W hvit</span><span class="volt" style="text-align: right;">kr. 640.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001019</span><span class="color">1200mm 18W nøytral</span><span class="volt" style="text-align: right;">kr. 640.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001020</span><span class="color">1200mm 18W varmhvit</span><span class="volt" style="text-align: right;">kr. 640.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001021</span><span class="color">1500mm 22W hvit</span><span class="volt" style="text-align: right;">kr. 720.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001022</span><span class="color">1500mm 22W nøytral</span><span class="volt" style="text-align: right;">kr. 720.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001023</span><span class="color">1500mm 22W varmhvit</span><span class="volt" style="text-align: right;">kr. 720.00</span></h4>
                </div>


           <!--
           <div class="entryhalf">
            	<h2>LED lenser H7 / H7R</h2>
              	<p class="productinfo">
                <img src="../images/produkter/ledlenserh7.jpg" class="left" style="border: 0;" />
              	Stilig lykt med kraftig lyskjegle. Lav vekt og pakkevolum. Trinnløs dimmer og fokus.
                <br /><br />
                H7 er batteridrevet med lysstyrke på 180lm og 80 timer driftstid. H7R er oppladbar, 200lm lysstyrke og 40 timer driftstid.

                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">&nbsp;</span><span class="volt header" style="text-align: right;">Pris</span></h4>
                 <h4 class="pricesmall"><span class="art">1001200</span><span class="color">H7</span><span class="volt" style="text-align: right;">kr. 450.00</span></h4>
                 <h4 class="pricesmall"><span class="art">1001205</span><span class="color">H7R</span><span class="volt" style="text-align: right;">kr. 660.00</span></h4>
            </div>
            <!-- entry end -->

            <!--
             <div class="entryhalf">
            	<h2>LED lenser H14 / H14R</h2>
              	<p class="productinfo">
                <img src="../images/produkter/ledlenserh14.jpg" class="left" style="border: 0;" />
              	Multifunksjonshodelykt som også kan brukes som sykkellykt, lommelykt eller lykt som festes i belte. Avansert fokuseringssystem, elektronisk dimmer, blink, SOS og strobefunksjon. Driftstid på mellom 5 og 25 timer, effekt på 220lm. Lader for USB og 230V medfølger, samt universalholder og skjøtekabel.

                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">&nbsp;</span><span class="volt header" style="text-align: right;">Pris</span></h4>
                 <h4 class="pricesmall"><span class="art">1001210</span><span class="color">H14</span><span class="volt" style="text-align: right;">kr. 860.00</span></h4>
                 <h4 class="pricesmall"><span class="art">1001215</span><span class="color">H14R</span><span class="volt" style="text-align: right;">kr. 1 250.00</span></h4>
            </div>
            <!-- entry end -->

           <!--
           <div class="entryhalf">
            	<h2>LED lenser M7R</h2>
              	<p class="productinfo">
                <img src="../images/produkter/ledlenserm7.jpg" class="left" style="border: 0;" />
              	Bestselgende håndlykt i oppladbar versjon. 200 lm med avansert fokussystem. Unikt magnetisk ladesystem som er enkelt å bruke med eller uten medfølgende veggefeste. Driftstid opp til 77 timer.

                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">&nbsp;</span><span class="volt header" style="text-align: right;">Pris</span></h4>
                 <h4 class="pricesmall"><span class="art">1001230</span><span class="color">&nbsp;</span><span class="volt" style="text-align: right;">kr. 990.00</span></h4>
            </div>
            <!-- entry end -->

            <!--
            <div class="entryhalf">
            	<h2>LED lenser B7</h2>
              	<p class="productinfo">
                <img src="../images/produkter/ledlenserb7.jpg" class="left" style="border: 0;" />
              	Kraftig sykkellykt med justerbar lyskjegle. Lykten kan tas ut av holderen og brukes som vanlig håndlykt. 200lm lysstyrke. Rekkevidde inntil 220 meter.

                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">&nbsp;</span><span class="volt header" style="text-align: right;">Pris</span></h4>
                 <h4 class="pricesmall"><span class="art">1001220</span><span class="color">&nbsp;</span><span class="volt" style="text-align: right;">kr. 620.00</span></h4>
            </div>
            <!-- entry end -->

           <!--
           <div class="entry">
            	<h2>NordLED retro "parafinlampe"</h2>
              	<p class="productinfo">
                <img src="../images/produkter/test01.jpg" class="left" style="border: 0;" />
                Blås i lampen for å tenne og slukke, stilig retro design som etterligner klassisk parafinlampe.
                Oppladbar 1W-LED, kan dimmes. Lyser opp til 8 timer per lading. Strømforsyning og lading via USB.
                <br /><br />
                 </p>
                  <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header">Pris</span></h4>
                  <h4 class="pricesmall"><span class="art">1001001</span><span class="color">&nbsp;</span><span class="volt">kr. 280.00</span></h4>
                <h4 class="spec">&nbsp;</h4>
                <h4 class="pricewide">&nbsp;</h4>
            </div>
            -->
            <!-- entry end -->

          <!-- <div class="entry">
            	<h2>NordLED 1 bordlampe med dimming</h2>
              	<p class="productinfo">
                <img src="../images/produkter/1001200.jpg" class="left" style="border: 0;" />
                Lekker bordlampe i støpt aluminium tilgjengelig i sølv eller gullfarget utførelse. Kapasitiv touchkontroll med 4 forhåndstinnstilte lysnivåer. Kan dimmes mellom 5-100% av lysstyrken.
                24V med transformator. 1200lux v 300mm høyde. Nøytral hvit, 4500K.
                <br /><br />
                 </p>
                  <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header">Pris</span></h4>
                  <h4 class="pricesmall"><span class="art">1001002</span><span class="color">sølv</span><span class="volt">kr. 600.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001003</span><span class="color">gull</span><span class="volt">kr. 600.00</span></h4>
                <h4 class="spec">&nbsp;</h4>
                <h4 class="pricewide">&nbsp;</h4>
            </div>
            <!-- entry end -->


            <!--<div class="entry">
             	<h2>NordLED 2 bordlampe med dimming</h2>
               	<p class="productinfo">
                 <img src="../images/produkter/ipost.jpg" class="left" style="border: 0;" />
                 Lekker bordlampe med kapasitiv touchkontroll. 2 fargetemperatrer, 5 forhåndstinnstilte lysnivåer. Kan dimmes mellom 5-100% av lysstyrken.
                 24V med transformator.
                 <br /><br />
                  </p>
                   <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header">Pris</span></h4>
                   <h4 class="pricesmall"><span class="art">1001004</span><span class="color">7W</span><span class="volt">kr. 500.00</span></h4>
                 <h4 class="spec">&nbsp;</h4>
                 <h4 class="pricewide">&nbsp;</h4>
             </div>
             <!-- entry end -->


          <!--
           <div class="entryhalf">
                       	<h2>NordLED 30W IR-SENSOR</h2>
                      	  <p class="productinfo">
                           <img src="../images/produkter/1001030.jpg" class="left" style="border: 0;" />
                          Lampehus i aluminium og herdet glass, værbestandig. IP54. Sammenlignet med tradisjonell lampe sparer man inntil 60%. Levetid på over 40 000 timer, RoHS- og CE-merket. Forbruk 0.4Amp 100-240V. Dekningsområde 180&deg;,deteksjonsavstand maks 12 meter, 100&deg; spredning.
                           <br /><br />


                           </p>
                           <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
                          <h4 class="pricesmall"><span class="art">1001005</span><span class="color">hvit</span><span class="volt" style="text-align: right;">kr. 790.00</span></h4>
                          <h4 class="pricesmall"><span class="art">1001006</span><span class="color">nøytral</span><span class="volt" style="text-align: right;">kr. 790.00</span></h4>
                          <h4 class="pricesmall"><span class="art">1001007</span><span class="color">varmhvit</span><span class="volt" style="text-align: right;">kr. 790.00</span></h4>
                          <h4 class="spec">-</h4>
                       </div>


           <div class="entryhalf">
                       	<h2>NordLED 50W IR-SENSOR</h2>
                      	  <p class="productinfo">
                           <img src="../images/produkter/1001050.jpg" class="left" style="border: 0;" />
                          Lampehus i aluminium og herdet glass, værbestandig. IP54. Sammenlignet med tradisjonell lampe sparer man inntil 60%. Levetid på over 40 000 timer, RoHS- og CE-merket. Forbruk 0.65Amp 100-240V. Dekningsområde 180&deg;,deteksjonsavstand maks 12 meter, 100&deg; spredning.
                           <br /><br />


                           </p>
                           <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
                          <h4 class="pricesmall"><span class="art">1001008</span><span class="color">hvit</span><span class="volt" style="text-align: right;">kr. 940.00</span></h4>
                          <h4 class="pricesmall"><span class="art">1001009</span><span class="color">nøytral</span><span class="volt" style="text-align: right;">kr. 940.00</span></h4>
                          <h4 class="pricesmall"><span class="art">1001010</span><span class="color">varmhvit</span><span class="volt" style="text-align: right;">kr. 940.00</span></h4>
                          <h4 class="spec">-</h4>
                       </div>
            -->

           <div class="entryhalf">
            	<h2>NordLED 3P-20W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/1001100-1215.jpg" class="left" style="border: 0;" />
                Solid LED-belysning med lang levetid. Ideel for bruk i garasje, verksted, lagerhaller o.l. IP65. 85-265V AC. Rustfri, vanntett og støvtett i aluminium og hardplast. Lengde 600mm. 1700lm, 140&deg; spredning.
                <br /><br />


                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">1001100</span><span class="color">hvit</span><span class="volt" style="text-align: right;">kr. 980.00</span></h4>
               <h4 class="pricesmall"><span class="art">1001101</span><span class="color">nøytral</span><span class="volt" style="text-align: right;">kr. 980.00</span></h4>
               <h4 class="pricesmall"><span class="art">1001102</span><span class="color">varmhvit</span><span class="volt" style="text-align: right;">kr. 980.00</span></h4>
               <h4 class="spec">-</h4>
            </div>

            <div class="entryhalf">
            	<h2>NordLED 3P-40W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/1001105-1215.jpg" class="left" style="border: 0;" />
                Solid LED-belysning med lang levetid. Ideel for bruk i garasje, verksted, lagerhaller o.l. IP65. 85-265V AC. Rustfri, vanntett og støvtett i aluminium og hardplast. Lengde 1200mm. 3400lm. 140&deg; spredning.
                <br /><br />

                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">1001105</span><span class="color">hvit</span><span class="volt" style="text-align: right;">kr. 1 080.00</span></h4>
               <h4 class="pricesmall"><span class="art">1001106</span><span class="color">nøytral</span><span class="volt" style="text-align: right;">kr. 1 080.00</span></h4>
               <h4 class="pricesmall"><span class="art">1001107</span><span class="color">varmhvit</span><span class="volt" style="text-align: right;">kr. 1 080.00</span></h4>
               <h4 class="spec">-</h4>
            </div>


            <div class="entryhalf">
            	<h2>NordLED 3P-50W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/1001110-1215.jpg" class="left" style="border: 0;" />
                Solid LED-belysning med lang levetid. Ideel for bruk i garasje, verksted, lagerhaller o.l. IP65. 85-265V AC. Rustfri, vanntett og støvtett i aluminium og hardplast. Lengde 1500mm. 4200lm. 140&deg; spredning.
                <br /><br />

                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">1001110</span><span class="color">hvit</span><span class="volt" style="text-align: right;">kr. 1 280.00</span></h4>
               <h4 class="pricesmall"><span class="art">1001111</span><span class="color">nøytral</span><span class="volt" style="text-align: right;">kr. 1 280.00</span></h4>
               <h4 class="pricesmall"><span class="art">1001112</span><span class="color">varmhvit</span><span class="volt" style="text-align: right;">kr. 1 280.00</span></h4>
               <h4 class="spec">-</h4>
            </div>

          <!--
           <div class="entryhalf">
            	<h2>NordLED COB Downlight 9W</h2>
              	<p class="productinfo">
                <img src="../images/produkter/1001025.jpg" class="left" style="border: 0;" />
                Downlight for innfelling, 45&deg; spredning. 95mm hull. Leveres med trafo.
                </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">1001025</span><span class="color">hvit</span><span class="volt">240V&nbsp;&nbsp;730lm</span></h4>
                 <h4 class="pricesmall"><span class="art">1001026</span><span class="color">nøytral</span><span class="volt">240V&nbsp;&nbsp;610lm</span></h4>
                 <h4 class="pricesmall"><span class="art">1001027</span><span class="color">varmhvit</span><span class="volt">240V&nbsp;&nbsp;640lm</span></h4>
                 <h4 class="spec">&nbsp;</h4>
                 <h4 class="price">kr. 420.00</h4>
            </div>
            


            <div class="entryhalf">
             	<h2>NordLED FDC Downlight 8W 4"</h2>
               	<p class="productinfo">
                 <img src="../images/produkter/1001030-1.jpg" class="left" style="border: 0;" />
                Downlight for innfelling, 100&deg; spredning. Leveres med trafo.

                  </p>
                  <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                  <h4 class="pricesmall"><span class="art">1001030</span><span class="color">hvit</span><span class="volt">240V&nbsp;&nbsp;500lm</span></h4>
                  <h4 class="pricesmall"><span class="art">1001031</span><span class="color">nøytral</span><span class="volt">240V&nbsp;&nbsp;490lm</span></h4>
                  <h4 class="pricesmall"><span class="art">1001032</span><span class="color">varmhvit</span><span class="volt">240V&nbsp;&nbsp;420lm</span></h4>
                  <h4 class="spec">&nbsp;</h4>
                  <h4 class="price">kr. 320.00</h4>
             </div>
              -->



           <!--
           <div class="entryhalf">
            	<h2>NordLED panel 2020&nbsp;&nbsp;10W</h2>
              	<p class="productinfo">
                <img src="../images/produkter/1001150.png" class="left" style="border: 0;" />
               LED-panel med 40.000 timers levetid, 105&deg; spredning. Ingen ventetid på lys, ingen summing. 220V/24V komplett med trafo.<br />

                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">1001150</span><span class="color">hvit</span><span class="volt">220V/24V&nbsp;&nbsp;619lm</span></h4>
                 <h4 class="pricesmall"><span class="art">1001151</span><span class="color">nøytral</span><span class="volt">220V/24V&nbsp;&nbsp;580lm</span></h4>
                 <h4 class="pricesmall"><span class="art">1001152</span><span class="color">varmhvit</span><span class="volt">220V/24V&nbsp;&nbsp;489lm</span></h4>
                 <h4 class="spec"><a href="../produktspesifikasjon/nordled_1001150.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
                 <h4 class="price">kr. 350.00</h4>
            </div>
             entry end -->
            <!--
            <div class="entryhalf">
            	<h2>NordLED panel 3030&nbsp;&nbsp;18W</h2>
              	<p class="productinfo">
                <img src="../images/produkter/1001151.png" class="left" style="border: 0;" />
                LED-panel med 40.000 timers levetid, 105&deg; spredning. Ingen ventetid på lys, ingen summing. 220V/24V komplett med trafo.
                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">1001160</span><span class="color">hvit</span><span class="volt">220V/24V&nbsp;&nbsp;1156lm</span></h4>
                 <h4 class="pricesmall"><span class="art">1001161</span><span class="color">nøytral</span><span class="volt">220V/24V&nbsp;&nbsp;1267lm</span></h4>
                 <h4 class="pricesmall"><span class="art">1001162</span><span class="color">varmhvit</span><span class="volt">220V/24V&nbsp;&nbsp;1046lm</span></h4>
                 <h4 class="spec"><a href="../produktspesifikasjon/nordled_1001160.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
                 <h4 class="price">kr. 500.00</h4>
            </div>
             entry end -->

             <!--
			      <div class="entry">
            	<h2>NordLED lysrør T8 - ta kontakt for produktsortiment</h2>
              	<p class="productinfo">
                <img src="../images/produkter/lysror.jpg" class="left" style="border: 0;" />
                Vi fører lysrør med både T8- og T5-sokkel. Leveres i lengder på 600, 1200 og 1500mm. Ta kontakt for mer informasjon.
                </p>

            </div>
            entry end -->



            <!-- entry end -->


          	<!-- <div class="entryhalf">
            	<h2>NordLED MR16 5W</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-001.jpg" class="left" style="border: 0;" />
                Spotpære som lyser tilsvarende vanlig 35W halogenspot. Utvikler svært lite varme, og har en levetid på ca 50 000 timer.<br />
                <br />
				<br />

                Sokkel GU5.3
                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">1001001</span><span class="color">hvit</span><span class="volt">12V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001002</span><span class="color">varmhvit</span><span class="volt">12V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001003</span><span class="color">nøytral</span><span class="volt">12V</span></h4>

                 <h4 class="spec"><a href="../produktspesifikasjon/nordled_mr16_5w_1001001.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
                 <h4 class="price">kr. 159.00</h4>
            </div>
            <!-- entry end -->


            <!--
            <div class="entryhalf">
            	<h2>NordLED dekorlys-360</h2>
              	<p class="productinfo">
                <img src="../images/produkter/1001004.jpg" class="left" style="border: 0;" />
                Dekorativ og stilig lampe som enkelt kan installeres både inne og ute. Perfekt til terrasse, brygge, rekkverk etc. IP67. Vanntett i støpt aluminium. 3W 0.25A<br />
                <br />
                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">1001050</span><span class="color">hvit</span><span class="volt">260lm&nbsp;&nbsp;&nbsp;12-24V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001051</span><span class="color">varmhvit</span><span class="volt">253lm&nbsp;&nbsp;&nbsp;12-24V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001052</span><span class="color">nøytral</span><span class="volt">230lm&nbsp;&nbsp;&nbsp;12-24V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001053</span><span class="color">rød</span><span class="volt">70lm&nbsp;&nbsp;&nbsp;12-24V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001054</span><span class="color">grønn</span><span class="volt">120lm&nbsp;&nbsp;&nbsp;12-24V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001055</span><span class="color">blå</span><span class="volt">40lm&nbsp;&nbsp;&nbsp;12-24V</span></h4>
                  <h4 class="spec"><a href="../produktspesifikasjon/1001301.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 360.00</h4>
            </div>
            -->
            <!-- entry end -->

            <!--
            <div class="entryhalf">
            	<h2>NordLED GU10 5W</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-004.jpg" class="left" style="border: 0;" />
                Spotpære som erstatter tradisjonell halogenspot. 5W lyser minst like godt som 40W halogenspot. Finnes i flere utførelser med forskjellig spredning.
                <br /><br />
				Sokkel GU10. Pæren leveres kun for 220V.
                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">1001005</span><span class="color">hvit</span><span class="volt">220V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001006</span><span class="color">varmhvit</span><span class="volt">220V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001007</span><span class="color">nøytral</span><span class="volt">220V</span></h4>
                 <h4 class="spec"><a href="../produktspesifikasjon/nordled_gu10_5w_1001005.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
                 <h4 class="price">kr. 159.00</h4>
            </div>
            <!-- entry end -->

            <!--
             <div class="entryhalf">
            	<h2>NordLED GU10 dimbar 5W</h2>
              	<p class="productinfo">
                <img src="../images/produkter/gu10dimny2012.jpg" class="left" style="border: 0;" />
                Spotpære som erstatter tradisjonell halogenspot. 5W lyser minst like godt som 50W halogenspot. Finnes i flere utførelser med forskjellig spredning. Fullt dimbar fra 0-100%.
                <br /><br />
				Sokkel GU10. Pæren leveres kun for 220V.
               </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">1001008</span><span class="color">nøytral</span><span class="volt">220V</span></h4>
               <h4 class="pricesmall"><span class="art">1001009</span><span class="color">varmhvit</span><span class="volt">220V</span></h4>
                 <h4 class="spec"><a href="../produktspesifikasjon/nordled_gu10dimbar_1001007.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
                 <h4 class="price">kr. 239.00</h4>
            </div>
            <!-- entry end -->



             <!--<div class="entryhalf">
            	<h2>NordLED SP6 6W</h2>
              	<p class="productinfo">
                <img src="../images/produkter/1001010-ny.jpg" class="left" style="border: 0;" />
               Erstatter vanlig 40W lyspære med E27 sokkel. Utvikler svært lite varme, og har en levetid på ca 50 000 timer. Fås i både hvit og varm hvit.
                <br /><br /><br />

                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">1001010</span><span class="color">hvit</span><span class="volt">220V - 550lm</span></h4>
                 <h4 class="pricesmall"><span class="art">1001011</span><span class="color">varmhvit</span><span class="volt">220V - 510lm</span></h4>
                 <h4 class="spec"><a href="../produktspesifikasjon/nordled_sp10_1001010.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
                 <h4 class="price">kr. 135.00</h4>
            </div>
            <!-- entry end -->

            <!--<div class="entryhalf">
            	<h2>NordLED SP10 10W</h2>
              	<p class="productinfo">
                <img src="../images/produkter/1001-012.jpg" class="left" style="border: 0;" />
               Erstatter vanlig 60W lyspære med E27 sokkel. Utvikler svært lite varme, og har en levetid på ca 50 000 timer. Fås i både hvit, nøytral og varm hvit.
                <br /><br /><br />

                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">1001012</span><span class="color">hvit</span><span class="volt">220V - 1050lm</span></h4>
                 <h4 class="pricesmall"><span class="art">1001013</span><span class="color">varmhvit</span><span class="volt">220V - 755lm</span></h4>
                 <h4 class="pricesmall"><span class="art">1001014</span><span class="color">nøytral</span><span class="volt">220V - 1000lm</span></h4>
                 <h4 class="spec">-</h4>
                 <h4 class="price">kr. 180.00</h4>
            </div>
            <!-- entry end -->

            <!--
            <div class="entryhalf">
            	<h2>NordLED Downlight 3W</h2>
              	<p class="productinfo">
                <img src="../images/produkter/1001040.jpg" class="left" style="border: 0;" />
               NB! Bildet avviker fra produktet.<br />
               Downlight for innfelling (40.6mm) 12V 3W. 45&deg; spredning.
                <br /><br /><br />

                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">1001040</span><span class="color">hvit</span><span class="volt">12V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001041</span><span class="color">varmhvit</span><span class="volt">12V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001042</span><span class="color">nøytral</span><span class="volt">12V</span></h4>
                 <h4 class="spec">-</a></h4>
                 <h4 class="price">kr. 195.00</h4>
            </div>
            <!-- entry end -->

            <!--
            <div class="entryhalf">
            	<h2>NordLED Downlight 8W 4" dimbar</h2>
              	<p class="productinfo">
                <img src="../images/produkter/1001400.jpg" class="left" style="border: 0;" />
               	Downlight for innfelling, dimbar med frostet glass. Sølvfarget. 68&deg; spredning.<br /><br />
                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">1001011</span><span class="color">nøytral</span><span class="volt">100-240V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001012</span><span class="color">varmhvit</span><span class="volt">100-240V</span></h4>
                 <h4 class="spec"><a href="../produktspesifikasjon/nordled_downlight_8w.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
                 <h4 class="price">kr. 340.00</h4>
            </div>
            <!-- entry end -->

            <!-- fjernet 30.10-12
            <div class="entryhalf">
            	<h2>NordLED leselys </h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-153.jpg" class="left" style="border: 0;" />
                Små interiørlamper 12V i flere farger. ØxH: 75x17mm. Både fargede og hvite led i samme lampe.
                Blått leselys for dag og rødt leselys for natt. Husk å oppgi farge ved bestilling
                <br /><br />

                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                  <h4 class="pricesmall"><span class="art">1001040</span><span class="color">blå</span><span class="volt">12V</span></h4>
                  <h4 class="pricesmall"><span class="art">1001041</span><span class="color">rød</span><span class="volt">12V</span></h4>
                 <h4 class="spec">-</h4><h4 class="price">kr. 229.00</h4>
            </div>
            -->
            <!-- entry end -->

            <!-- fjernet 30.10.12
            <div class="entryhalf">
            	<h2>NordLED ledelys/markørlys </h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-152.jpg" class="left" style="border: 0;" />
                Små interiørlamper 12V i flere farger. Størrelse: 100x25mm. Hardplast deksel. 10cm kabel.
                Perfekt for hytter, båter, campingvogner og biler.
                Husk å oppgi farge ved bestilling.
                <br /><br />
                God spredning 120&deg;<br />


                <br />

                 </p>
                  <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                  <h4 class="pricesmall"><span class="art">1001045</span><span class="color">grønn</span><span class="volt">12V / 0.58amp</span></h4>
                  <h4 class="pricesmall"><span class="art">1001046</span><span class="color">rød</span><span class="volt">12V / 0.58amp</span></h4>
                  <h4 class="pricesmall"><span class="art">1001047</span><span class="color">blå</span><span class="volt">12V / 0.58amp</span></h4>
                  <h4 class="spec">-</h4><h4 class="price">kr. 179.00</h4>
            </div>
            -->
            <!-- entry end -->
            <!--
            <div class="entryhalf">
            	<h2>NordLED lyslist 500</h2>
              	<p class="productinfo">
                <img src="../images/produkter/ark4.jpg" class="left" style="border: 0;" />
                En ny type belysning for bruk der tradisjonell belysning er upraktisk. Lyslisten kan brukes i båt, hytter, hus og som dekorasjonbelysning både inne og ute. Leveres i hvit og varm hvit. 96 lyspunkter fordelt på 500mm lyslist komplett med trafo.
                <br /><br />
                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">1001025</span><span class="color">hvit</span><span class="volt">24V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001026</span><span class="color">varmhvit</span><span class="volt">24V</span></h4>
                 <h4 class="spec">-</h4><h4 class="price">kr. 529.00</h4>
            </div>
            <!-- entry end -->

            <!-- fjernet 03.11.12
            <div class="entryhalf">
            	<h2>NordLED interiørbelysning</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-150.jpg" class="left" style="border: 0;" />
                Små interiørlamper i 12V eller 24V i flere farger. Størrelse: 65x30mm. Forkrommet metall i lampefeste og hardplast deksel. 14cm kabel.
                Perfekt for hytter, båter, campingvogner og biler.
                Husk å oppgi farge ved bestilling.
                <br /><br />
                 </p>

                  <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                  <h4 class="pricesmall"><span class="art">1001050</span><span class="color">blå</span><span class="volt">12V</span></h4>
                  <h4 class="pricesmall"><span class="art">1001051</span><span class="color">rød</span><span class="volt">12V</span></h4>
                  <h4 class="pricesmall"><span class="art">1001052</span><span class="color">grønn</span><span class="volt">12V</span></h4>
                  <h4 class="pricesmall"><span class="art">1001053</span><span class="color">hvit</span><span class="volt">12V</span></h4>
                  <h4 class="pricesmall"><span class="art">1001055</span><span class="color">blå</span><span class="volt">24V</span></h4>
                  <h4 class="pricesmall"><span class="art">1001056</span><span class="color">rød</span><span class="volt">24V</span></h4>
                  <h4 class="pricesmall"><span class="art">1001057</span><span class="color">grønn</span><span class="volt">24V</span></h4>
                  <h4 class="pricesmall"><span class="art">1001058</span><span class="color">hvit</span><span class="volt">24V</span></h4>
                  <h4 class="spec">-</h4><h4 class="price">kr. 179.00</h4>
            </div>
            -->
            <!-- entry end -->

            <!-- fjernet 03.11.12
            <div class="entryhalf">
            	<h2>NordLED interiørbelysning</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-120.jpg" class="left" style="border: 0;" />
                Disse små LED-lamper tilsvarer dagens glødepærer på 15W. Leveres i krom, gull og hvitt. ØxH: 75x17mm. Leveres i 12V eller 24V.
                Perfekt for hytter, båter, campingvogner og biler.
                Husk å oppgi farge ved bestilling.
                <br /><br />

                 </p>
                   <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                  <h4 class="pricesmall"><span class="art">1001061</span><span class="color">krom</span><span class="volt">12V</span></h4>
                  <h4 class="pricesmall"><span class="art">1001062</span><span class="color">gull</span><span class="volt">12V</span></h4>
                  <h4 class="pricesmall"><span class="art">1001063</span><span class="color">hvit</span><span class="volt">12V</span></h4>
                  <h4 class="pricesmall"><span class="art">1001065</span><span class="color">krom</span><span class="volt">24V</span></h4>
                  <h4 class="pricesmall"><span class="art">1001066</span><span class="color">gull</span><span class="volt">24V</span></h4>
                  <h4 class="pricesmall"><span class="art">1001067</span><span class="color">hvit</span><span class="volt">24V</span></h4>
                 <h4 class="spec"><a href="../produktspesifikasjon/nordled_interiorbelysning_1001061.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
                 <h4 class="price">kr. 239.00</h4>

            </div>
            -->
            <!-- entry end -->


          <div style="float: left; clear: both; width: 700px;">&nbsp;</div>

          <div class="entry">
            	<h2>NordLED lyslist</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-124-wide.jpg" class="left" style="border: 0;" />
                Disse små lyssterke LED-lyslister passer til hus, hytter, biler og båter. Slank utførelse, kun 10mm høy og 25mm bred. Leveres i lengder 150mm, 300mm og 600mm. Leveres kun i hvitt med 12V eller 24V.
                Lysstyrken på 150mm tilsvarer 15W glødepære, 300mm tilsvarer 30W glødepære og 600mm tilsvarer 60W glødepære.
                <br /><br />
                 </p>
                  <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header">Pris</span></h4>
                  <h4 class="pricesmall"><span class="art">1001070</span><span class="color">150mm 12V</span><span class="volt">kr. 319.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001071</span><span class="color">300mm 12V</span><span class="volt">kr. 480.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001072</span><span class="color">600mm 12V</span><span class="volt">kr. 619.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001075</span><span class="color">150mm 24V</span><span class="volt">kr. 680.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001076</span><span class="color">300mm 24V</span><span class="volt">kr. 480.00</span></h4>
                  <h4 class="pricesmall"><span class="art">1001077</span><span class="color">600mm 24V</span><span class="volt">kr. 680.00</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_lyslist_1001070.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
                <h4 class="pricewide">-</h4>
            </div>
            <!-- entry end -->

          <!--
          <div class="entryhalf">
            	<h2>NordLED-remse 5m 24V</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-010.jpg" class="left" style="border: 0;" />
                Fleksibel remse som kan klippes ved hver 6. LED (50mm), 3M selvklebende teip på baksiden, bredde 16mm, tykkelse 3mm. IP65. Leveres med lys i hvit, varm-hvit og nøytral hvit. Leveres også på rull á 30m.
                <br />

                 </p>
                  <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                  <h4 class="pricesmall"><span class="art">1001030</span><span class="color">hvit</span><span class="volt">24V</span></h4>
                  <h4 class="pricesmall"><span class="art">1001031</span><span class="color">varmhvit</span><span class="volt">24V</span></h4>
                  <h4 class="pricesmall"><span class="art">1001032</span><span class="color">nøytral</span><span class="volt">24V</span></h4>
                  <h4 class="spec"><a href="../produktspesifikasjon/nordled_remse5m_1001030.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
                  <h4 class="price">kr. 1 249.00</h4>
            </div>
            <!-- entry end -->

            <!--
            <div class="entryhalf">
            	<h2>NordLED-remse 5m RGB</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-011.jpg" class="left" style="border: 0;" />
                Fleksibel remse som kan klippes ved hver 3. LED, 3M selvklebende teip på baksiden, bredde 14mm, tykkelse 3mm. 15 forskjellige blinkmønstre, variabel innstilling for hastighet.<br />

                <br />
                 </p>
                   <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                  <h4 class="pricesmall"><span class="art">1001035</span><span class="color">rgb</span><span class="volt">12V</span></h4>
                  <h4 class="spec">-</h4><h4 class="price">kr. 1 999.00</h4>
            </div>
            <!-- entry end -->

            <!--
             <div class="entryhalf">
            	<h2>NordLED lampe rund 6" 11W</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-007.jpg" class="left" style="border: 0;" />
               	Lampe med 105&deg;-spredning, 40.000 timers levetid med 50% energiinnsparing i forhold til tradisjonell 11W "sparepære". IP43. 220V/24V, leveres komplett med trafo.
                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">1001015</span><span class="color">hvit</span><span class="volt">220V/24V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001016</span><span class="color">varmhvit</span><span class="volt">220V/24V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001017</span><span class="color">nøytral</span><span class="volt">220V/24V</span></h4>
                 <h4 class="spec"><a href="../produktspesifikasjon/nordled_lamperund_1001015.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
                 <h4 class="price">kr. 599.00</h4>
            </div>
            <!-- entry end -->
            <!--
             <div class="entryhalf">
            	<h2>NordLED lampe rund 8" 14W</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-009.jpg" class="left" style="border: 0;" />
               	Lampe med 105°-spredning, 40.000 timers levetid med 50% energiinnsparing i forhold til tradisjonell 14W "sparepære". IP43. 220V/24V, leveres komplett med trafo.
                 </p>
                  <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">1001020</span><span class="color">hvit</span><span class="volt">220V/24V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001021</span><span class="color">varmhvit</span><span class="volt">220V/24V</span></h4>
                 <h4 class="pricesmall"><span class="art">1001022</span><span class="color">nøytral</span><span class="volt">220V/24V</span></h4>
                 <h4 class="spec"><a href="../produktspesifikasjon/nordled_lamperund_1001020.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
                 <h4 class="price">kr. 649.00</h4>
            </div>
            <!-- entry end -->




      		<!-- fjernet 30.10.12
            <div class="entryhalf">
            	<h2>NordLED-G4 flat 12V 1W</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-132.jpg" class="left" style="border: 0;" />
                Erstatter vanlig halogenspot med G4-sokkel. Tilsvarer ca 10W halogenspot. Svært lav varmeutvikling, tåler vibrasjon og støt og har lang levetid.
                <br /><br /><br />

                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">1001085</span><span class="color">hvit</span><span class="volt">12V 1W 74lm</span></h4>
                 <h4 class="pricesmall"><span class="art">1001086</span><span class="color">varmhvit</span><span class="volt">12V 1W 48lm</span></h4>
                 <h4 class="spec"><a href="../produktspesifikasjon/nordled_g4flat_1001085.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
                 <h4 class="price">kr. 64.00</h4>
            </div>
            -->
            <!-- entry end -->

          	<!-- fjernet 30.10.12
          	<div class="entryhalf">
            	<h2>NordLED-G4 rund 12V 1.6W</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-133.jpg" class="left" style="border: 0;" />
                Erstatter vanlig halogenspot med G4-sokkel. Tilsvarer ca 10W halogenspot. Svært lav varmeutvikling, tåler vibrasjon og støt og har lang levetid.
                <br /><br /><br />

                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">1001080</span><span class="color">hvit</span><span class="volt">12V 1.6W 110lm</span></h4>
                 <h4 class="pricesmall"><span class="art">1001081</span><span class="color">varmhvit</span><span class="volt">12V 1.6W 76lm</span></h4>
                 <h4 class="spec"><a href="../produktspesifikasjon/nordled_g4rund_1001080.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
                 <h4 class="price">kr. 84.00</h4>
            </div>
            -->
            <!-- entry end -->


        </div><!-- content end-->
	</div>
</div>

<?php require("../include/footer.php");?>


<script type="text/javascript"> Cufon.now(); </script>

</body>
</html>
