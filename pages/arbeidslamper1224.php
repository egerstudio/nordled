<?php
$page = "arbeidslamper1";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LED-shop Norge</title>
<link href="../css/common.css" rel="stylesheet" type="text/css" />
<?php require("../include/javascripts.php");?>
</head>
<body>
<div id="wrap">

    <div id="page">
    	<?php require("../include/top.php");?>

        <div id="content">
        	<h1>Arbeidslys 12/24V</h1>
            <h2>alle priser er inklusiv merverdiavgift</h2>


            <div class="entryhalf">
              <h2>NordLED 6610 FF</h2>
                <p class="productinfo">
                <img src="../images/produkter/3001025.jpg" class="left" style="border: 0;" /><br />
                <img src="../images/produkter/3001025-2.jpg" class="left" style="border: 0;" />
                Multivolt arbeidslampe 900lm. 1x10W LED. Forbruk ved 12V 0.9A, forbruk ved 24V 0.4A. Dimensjon:  120x66x65mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Lys</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001025 </span><span class="color">Flomlys</span><span class="volt">12/24V&nbsp;&nbsp;&nbsp;IP67<span class="dim"></span></span></h4>
                <h4 class="spec">&nbsp;</h4><h4 class="price">kr. 660.00</h4>
            </div>

            <div class="entryhalf">
              <h2>NordLED 6610 FS</h2>
                <p class="productinfo">
                <img src="../images/produkter/3001026.jpg" class="left" style="border: 0;" /><br />
                <img src="../images/produkter/3001026-2.jpg" class="left" style="border: 0;" />
                Multivolt arbeidslampe 900lm. 1x10W LED. Forbruk ved 12V 0.9A, forbruk ved 24V 0.4A. Dimensjon:  120x66x65mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Lys</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001026 </span><span class="color">Spotlys</span><span class="volt">12/24V&nbsp;&nbsp;&nbsp;IP67<span class="dim"></span></span></h4>
                <h4 class="spec">&nbsp;</h4><h4 class="price">kr. 660.00</h4>
            </div>



            <div class="entryhalf">
              <h2>NordLED R rygge- &amp; arbeidslys</h2>
                <p class="productinfo">
                <img src="../images/produkter/3001014.jpg" class="left" style="border: 0;" /><br />
                <img src="../images/produkter/3001014-2.jpg" class="left" style="border: 0;" />
                Multivolt arbeidslampe 1440lm. 6x3W LED. Forbruk ved 12V 1.46A, forbruk ved 24V 0.7A. Dimensjon:  Ø89x58mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Lys</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001009 </span><span class="color">Flomlys</span><span class="volt">12/24V&nbsp;&nbsp;&nbsp;IP67<span class="dim"></span></span></h4>
                <h4 class="spec">&nbsp;</h4><h4 class="price">kr. 1 060.00</h4>
            </div>


            <div class="entryhalf">
              <h2>NordLED 1200 FF</h2>
                <p class="productinfo">
                <img src="../images/produkter/3001020.jpg" class="left" style="border: 0;" /><br />
                <img src="../images/produkter/3001020-1.jpg" class="left" style="border: 0;" />
                Multivolt arbeidslampe 1200lm. 5x3W LED. Forbruk ved 12V 1.3A, forbruk ved 24V 0.6A. Dimensjon: 128x110x45mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Lys</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001010 </span><span class="color">Flomlys</span><span class="volt">12/24V&nbsp;&nbsp;&nbsp;IP67<span class="dim"></span></span></h4>
                <h4 class="spec">&nbsp;</h4><h4 class="price">kr. 660.00</h4>
            </div>


            <div class="entryhalf">
              <h2>NordLED 2160 FF</h2>
                <p class="productinfo">
                <img src="../images/produkter/3001007.jpg" class="left" style="border: 0;" /><br />
                <img src="../images/produkter/3001007-1.jpg" class="left" style="border: 0;" />
                <p>
                Multivolt arbeidslampe 2160lm. 9x3W, forbruk ved 12V 1.8A, forbruk ved 24V 0.9A. Dimensjon: 128x110x55mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Lys</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001007 </span><span class="color">Flomlys</span><span class="volt">12/24V&nbsp;&nbsp;&nbsp;IP67<span class="dim"></span></span></h4>
                <h4 class="spec">&nbsp;</h4><h4 class="price">kr. 1060.00</h4>
            </div>


            <div class="entryhalf">
              <h2>NordLED 2160 RF</h2>
                <p class="productinfo">
                <img src="../images/produkter/3001008.jpg" class="left" style="border: 0;" /><br />
                <img src="../images/produkter/3001008-1.jpg" class="left" style="border: 0;" />
                <p>
                Multivolt arbeidslampe 2160lm. 9x3W, forbruk ved 12V 1.8A, forbruk ved 24V 0.9A. Dimensjon: Ø128x55mm.<br ><br>
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Lys</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001008 </span><span class="color">Flomlys</span><span class="volt">12/24V&nbsp;&nbsp;&nbsp;IP67<span class="dim"></span></span></h4>
                <h4 class="spec">&nbsp;</h4><h4 class="price">kr. 1 060.00</h4>
            </div>



            <div class="entryhalf">
              <h2>NordLED 3500 FF</h2>
                <p class="productinfo">
                <img src="../images/produkter/3001013.jpg" class="left" style="border: 0;" /><br />
                <img src="../images/produkter/3001013-1.jpg" class="left" style="border: 0;" />
                <p>
                Multivolt arbeidslampe 3500lm. 7x5W, forbruk ved 12V 2.03A, forbruk ved 24V 1.06A. Dimensjon: 130x112x74mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Lys</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001003 </span><span class="color">Flomlys</span><span class="volt">12/24V&nbsp;&nbsp;&nbsp;IP67<span class="dim"></span></span></h4>
                <h4 class="spec">&nbsp;</h4><h4 class="price">kr. 2 130.00</h4>
            </div>


            <div class="entryhalf">
              <h2>NordLED 3780 FS</h2>
                <p class="productinfo">
                <img src="../images/produkter/3001027.jpg" class="left" style="border: 0;" /><br />
                <img src="../images/produkter/3001027-1.jpg" class="left" style="border: 0;" />
                <p>
                Multivolt arbeidslampe 3780lm. 9x5W, forbruk ved 12V 3.3A, forbruk ved 24V 1.7A. Dimensjon: 156x126x95mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Lys</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001011 </span><span class="color">Spotlys</span><span class="volt">12/24V&nbsp;&nbsp;&nbsp;IP67<span class="dim"></span></span></h4>
                <h4 class="spec">&nbsp;</h4><h4 class="price">kr. 2 880.00</h4>
            </div>




            <div class="entryhalf">
              <h2>NordLED 8100 FF</h2>
                <p class="productinfo">
                <img src="../images/produkter/3001018.jpg" class="left" style="border: 0;" /><br />
                <img src="../images/produkter/3001018-1.jpg" class="left" style="border: 0;" />
                <p>
                Multivolt arbeidslampe 8100lm. 9x10W, forbruk ved 12V 6.4A, forbruk ved 24V 3.1A. Dimensjon: 160x135x124mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Lys</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001012 </span><span class="color">Flomlys</span><span class="volt">12/24V&nbsp;&nbsp;&nbsp;IP67<span class="dim"></span></span></h4>
                <h4 class="spec">&nbsp;</h4><h4 class="price">kr. 4 380.00</h4>
            </div>


            <div class="entryhalf">
              <h2>NordLED 8100 FS</h2>
                <p class="productinfo">
                <img src="../images/produkter/3001019.jpg" class="left" style="border: 0;" /><br />
                <img src="../images/produkter/3001019-1.jpg" class="left" style="border: 0;" />
                <p>
                Multivolt arbeidslampe 8100lm. 9x10W, forbruk ved 12V 6.4A, forbruk ved 24V 3.1A. Dimensjon: 160x135x124mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Lys</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001013 </span><span class="color">Spotlys</span><span class="volt">12/24V&nbsp;&nbsp;&nbsp;IP67<span class="dim"></span></span></h4>
                <h4 class="spec">&nbsp;</h4><h4 class="price">kr. 4 380.00</h4>
            </div>








            <div class="entryhalf">
              <h2>NordLED arbeidslys 4-LED FF</h2>
                <p class="productinfo">
                <img src="../images/produkter/3001001.jpg" class="left" style="border: 0;" /><br />
                <img src="../images/produkter/3001001-1.jpg" class="left" style="border: 0;" />
                Multivolt flom arbeidslampe 960lm. 4x3W, forbruk ved 12V 0.9A, forbruk ved 24V 0.4A. Dimensjon: 120x66x65mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Lys</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001001 </span><span class="color">Flomlys</span><span class="volt">12/24V&nbsp;&nbsp;&nbsp;IP67<span class="dim"></span></span></h4>
                <h4 class="spec">&nbsp;</h4><h4 class="price">kr. 770.00</h4>
            </div>


            <div class="entryhalf">
              <h2>NordLED arbeidslys 4-LED FS</h2>
                <p class="productinfo">
                <img src="../images/produkter/3001002.jpg" class="left" style="border: 0;" /><br />
                <img src="../images/produkter/3001002-1.jpg" class="left" style="border: 0;" />
                Multivolt spot arbeidslampe 960lm. 4x3W, forbruk ved 12V 0.9A, forbruk ved 24V 0.4A. Dimensjon: 120x66x65mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Lys</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001002 </span><span class="color">Spotlys</span><span class="volt">12/24V&nbsp;&nbsp;&nbsp;IP67<span class="dim"></span></span></h4>
                <h4 class="spec">&nbsp;</h4><h4 class="price">kr. 770.00</h4>
            </div>


          	<!--<div class="entryhalf">
            	<h2>NordLED R48 - 48W</h2>
              	<p class="productinfo">
                <img src="../images/produkter/r48.jpg" class="left" style="border: 0;" />
                Arbeidslys for svært krevende bruksområder. Høyintensitets LED-dioder som yter 4800lm med over 50 000 timers driftstid i et vær- og vibrasjonsbestandig lampehus av støpt aluminium. Svært driftssikker og et meget godt lys.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001115</span><span class="color">-</span><span class="volt">11-70V&nbsp;&nbsp;&nbsp;IP68 <span class="dim"></span></span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys_r48.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 2 000.00</h4>
            </div>
            <!-- entry end -->
            <!--
            <div class="entryhalf">
            	<h2>NordLED F32 - 32W</h2>
              	<p class="productinfo">
                <img src="../images/produkter/f32.jpg" class="left" style="border: 0;" />
                Arbeidslys for svært krevende bruksområder. Høyintensitets LED-dioder som yter 3200lm med over 50 000 timers driftstid i et vær- og vibrasjonsbestandig lampehus av støpt aluminium. Svært driftssikker og et meget godt lys.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001114</span><span class="color">-</span><span class="volt">11-70V &nbsp;&nbsp;&nbsp;IP68<span class="dim"></span></span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys_f32.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 1 800.00</h4>
            </div>
            <!-- entry end -->

           <!--
           <div class="entryhalf">
            	<h2>NordLED PAR36</h2>
              	<p class="productinfo">
                <img src="../images/produkter/par36.jpg" class="left" style="border: 0;" />
                Arbeidslys for svært krevende bruksområder. 10-50V og levetid på over 30.000 timer. Vann- og værbestandig lampehus, IP67. 1000lm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001118</span><span class="color">-</span><span class="volt">10-50V &nbsp;&nbsp;&nbsp;IP67<span class="dim"></span></span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys_par36.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 1 150.00</h4>
            </div>
            <!-- entry end -->












            <!--
          	<div class="entryhalf">
            	<h2>NordLED arbeidslys 6</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-039.jpg" class="left" style="border: 0;" />
                12/24V arbeidslys med 1 meter kabel.
                3715lx på 1-meters avstand.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001001</span><span class="color">-</span><span class="volt">12/24V <span class="dim"></span></span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys6_3001001.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 980.00</h4>
            </div>
            <!-- entry end -->
            <!--
            <div class="entryhalf">
            	<h2>NordLED arbeidslys 8</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-040.jpg" class="left" style="border: 0;" />
                12/24V arbeidslys med 1 meter kabel.
                4630lx på 1-meters avstand.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001002</span><span class="color">-</span><span class="volt">12/24V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys8_3001002.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 1 190.00</h4>
            </div>
            <!-- entry end -->

            <!--
            <div class="entryhalf">
            	<h2>NordLED arbeidslys 14</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-041.jpg" class="left" style="border: 0;" />
                12/24V arbeidslys med 1 meter kabel.
                8460lx på 1-meters avstand. <br />
<br />

                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001003</span><span class="color">-</span><span class="volt">12/24V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys14_3001003.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 1 680.00</h4>
            </div>
            <!-- entry end -->



            <div class="entryhalf">
            	<h2>NordLED 175 F</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001005.jpg" class="left" style="border: 0;" />
                <img src="../images/produkter/3001005-1.jpg" class="left" style="border: 0;" />
                En liten men kraftig lampe med 5 stk LED, og minimalt strømforbruk. Multivolt 12/24V, hus i aluminium og hardplast.
                Leveres i sort. <br  />
                Dimensjon: 114x78x71mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001005</span><span class="color">-</span><span class="volt">12/24V</span></h4>
                <h4 class="spec">&nbsp;</h4><h4 class="price">kr. 779.00</h4>
            </div>
            <!-- entry end -->


            <div class="entryhalf">
            	<h2>NordLED 1080 F</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001006.jpg" class="left" style="border: 0;" />
                <img src="../images/produkter/3001006-1.jpg" class="left" style="border: 0;" />
                En liten men kraftig lampe med 9 stk LED, og minimalt strømforbruk. Multivolt 12/24V, hus i aluminium og hardplast.
                Leveres i sort.  <br  />
                Dimensjon: 169x60x77mm.
                <br />
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001006</span><span class="color">-</span><span class="volt">12/24V</span></h4>
                <h4 class="spec">&nbsp;</h4><h4 class="price">kr. 979.00</h4>
            </div>
            <!-- entry end -->





            <!--
            <div class="entryhalf">
            	<h2>NordLED arbeidslys R 12-48V</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001007.jpg" class="left" style="border: 0;" />
                En liten men kraftig lampe med 4 stk LED, minimalt strømforbruk. Lampehus i aluminium og hardplast. Leveres i sort. IP66. Forbruk ved 12V 0.9A, forbruk ved 48V 0.3A.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001007</span><span class="color">-</span><span class="volt">12-48V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys_r.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 980.00</h4>
            </div>
             entry end -->
            <!--
            <div class="entryhalf">
            	<h2>NordLED arbeidslys F 12-48V</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001008.jpg" class="left" style="border: 0;" />
                En liten men kraftig lampe med 4 stk LED, minimalt strømforbruk. Lampehus i aluminium og hardplast. Leveres i sort. IP66. Forbruk ved 12V 0.9A, forbruk ved 48V 0.3A.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001008</span><span class="color">-</span><span class="volt">12-48V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys_f.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 980.00</h4>
            </div>
             entry end -->

						<!--
            <div class="entryhalf">
                        	<h2>NordLED SY-5 LED&nbsp;&nbsp;12V</h2>
                          	<p class="productinfo">
                            <img src="../images/produkter/3001116.jpg" class="left" style="border: 0;" />
                            Arbeidslampe med 5 LED-punkter, 15W, 500lm. Lampehus i aluminium og feste i rustfritt stål. Vann-, vibrasjon- og støvbestandig.<br />
            <br />

                            </p>
                            <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                            <h4 class="pricesmall"><span class="art">3001117</span><span class="color">hvit</span><span class="volt">12V</span></h4>
                            <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys_3001117.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 480.00</h4>
                        </div>
                        <!-- entry end -->

												<!--
            <div class="entryhalf">
                        	<h2>NordLED SY-6 LED&nbsp;&nbsp;12V</h2>
                          	<p class="productinfo">
                            <img src="../images/produkter/3001115.jpg" class="left" style="border: 0;" />
                            Arbeidslampe med 6 LED-punkter, 15W, 500lm. Lampehus i aluminium og feste i rustfritt stål. Vann-, vibrasjon- og støvbestandig.<br />
            <br />

                            </p>
                            <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                            <h4 class="pricesmall"><span class="art">3001116</span><span class="color">hvit</span><span class="volt">12V</span></h4>
                            <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys_3001116.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 480.00</h4>
                        </div>
                        <!-- entry end -->



            <div class="entryhalf">
            	<h2>NordLED 1000 4F</h2>
              	<p class="productinfo">
                <img src="../images/produkter/nordledmulti4f.jpg" class="left" style="border: 0;" />
                <img src="../images/produkter/nordledmulti4f-1.jpg" class="left" style="border: 0;" />
                 Multivolt arbeidslampe 1000lm. 4x3W LED, forbruk ved 12V 0.1A, forbruk ved 24V 0.05A. Dimensjon: 81x110x67mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001014</span><span class="color">-</span><span class="volt">12/24V&nbsp;&nbsp;IP67<span class="dim"></span></span></h4>
                <h4 class="spec">&nbsp;</h4><h4 class="price">kr. 860.00</h4>
            </div>

            <div class="entryhalf">
            	<h2>NordLED 1000 4R</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001022.jpg" class="left" style="border: 0;" />
                <img src="../images/produkter/3001022-1.jpg" class="left" style="border: 0;" />
                 Multivolt arbeidslampe 1000lm. 4x3W LED, forbruk ved 12V 0.1A, forbruk ved 24V 0.05A. Dimensjon: 88x157x55mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001015</span><span class="color">-</span><span class="volt">12/24V&nbsp;&nbsp;IP67<span class="dim"></span></span></h4>
                <h4 class="spec">&nbsp;</h4><h4 class="price">kr. 860.00</h4>
            </div>


            <!--
            <div class="entryhalf">
            	<h2>NordLED Multi 4R</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001111.jpg" class="left" style="border: 0;" />
                Multivolt arbeidslampe med 4 LED-punkter på 3W, 1400lx.
                <br /><br />

                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001111</span><span class="color">-</span><span class="volt">10-30V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_3001111.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 680.00</h4>
            </div>
            <!-- entry end -->

             <div class="entryhalf">
            	<h2>NordLED Multi 6</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001112.jpg" class="left" style="border: 0;" />
                <img src="../images/produkter/3001112-1.jpg" class="left" style="border: 0;" />
                Multivolt arbeidslampe med 6 LED-punkter på 3W, 1500lm. Dimensjon: 122x157x90mm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001016</span><span class="color">-</span><span class="volt">10/24V&nbsp;&nbsp;IP67</span></h4>
                <h4 class="spec">&nbsp;</h4><h4 class="price">kr. 900.00</h4>
            </div>
            <!-- entry end -->

             <div class="entryhalf">
            	<h2>NordLED Multi 8</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001113.jpg" class="left" style="border: 0;" />
                <img src="../images/produkter/3001113-1.jpg" class="left" style="border: 0;" />
                Multivolt arbeidslampe med 8 LED-punkter på 3W, 2000lm. Dimensjon: 143x120x65mm.
                </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                 <h4 class="pricesmall"><span class="art">3001017</span><span class="color">-</span><span class="volt">10/24V&nbsp;&nbsp;IP67</span></h4>
                 <h4 class="spec">&nbsp;</h4><h4 class="price">kr. 960.00</h4>
            </div>
            <!-- entry end -->


            <div class="entryhalf">
             <h2>NordLED 1440 FF</h2>
               <p class="productinfo">
               <img src="../images/produkter/3001018-1215.jpg" class="left" style="border: 0;" />
               <img src="../images/produkter/3001026-1.jpg" class="left" style="border: 0;" />
               Multivolt arbeidslampe 1440lm. 6x3W, forbruk ved 12V 1.42A, forbruk ved 24V 0.7A. Dimensjon: 160x45x63mm.
               </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001018</span><span class="color">-</span><span class="volt">10/24V&nbsp;&nbsp;IP67</span></h4>
                <h4 class="spec">&nbsp;</h4><h4 class="price">kr. 960.00</h4>
           </div>
           <!-- entry end -->





       <div class="entry">
          <h2>NordLED 5400 LS</h2>
            <img src="../images/produkter/3001132w.jpg" class="left" style="border: 0;" />
            <p class="productinfo">
            <img src="../images/produkter/3001132-1.jpg" style="padding-right: 20px;">
            Multivolt arbeidslampe 5400lm. 6x10W
            <br />
            Forbruk ved 12V 4.3A, forbruk ved 24V 2.1A.
            <br /><br />
            Mål (LxBxH): 289x90x96mm.
            <br><br>
            Komplett med relé, lang kabel og bryter.
             </p>
              <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header">Pris</span></h4>
              <h4 class="pricesmall"><span class="art">3001020</span><span class="color">12/24V&nbsp;&nbsp;IP67</span><span class="volt">kr. 3 580.00</span></h4>
            <h4 class="spec">&nbsp;</h4>
            <h4 class="pricewide">&nbsp;</h4>
        </div>
        <!-- entry end -->


        <div class="entry">
           <h2>NordLED 10800 LS</h2>
             <img src="../images/produkter/3001133w.jpg" class="left" style="border: 0;" />
             <p class="productinfo">
             <img src="../images/produkter/3001133-1.jpg" style="padding-right: 20px;">
             Multivolt arbeidslampe 10 800lm. 12x10W.
             <br />
             Forbruk ved 12V 7.8A, forbruk ved 24V 3.1A.
             <br /><br />
             Mål (LxBxH): 543x90x96mm.
             <br><br>
             Komplett med relé, lang kabel og bryter.
              </p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">3001021</span><span class="color">12/24V&nbsp;&nbsp;IP67</span><span class="volt">kr. 6 080.00</span></h4>
             <h4 class="spec">&nbsp;</h4>
             <h4 class="pricewide">&nbsp;</h4>
         </div>
         <!-- entry end -->


         <div class="entry">
            <h2>NordLED 16200 LS</h2>
              <img src="../images/produkter/3001134w.jpg" class="left" style="border: 0;" />
              <p class="productinfo">
              <img src="../images/produkter/3001134-1.jpg" style="padding-right: 20px;">
              Multivolt arbeidslampe 16 200lm. 18x10W.
              <br />
              Forbruk ved 12V 9.8A, forbruk ved 24V 4.9A
              <br /><br />
              Mål (LxBxH): 797x90x96mm.
              <br><br>
              Komplett med relé, lang kabel og bryter.
               </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header">Pris</span></h4>
                <h4 class="pricesmall"><span class="art">3001022</span><span class="color">12/24V&nbsp;&nbsp;IP67</span><span class="volt">kr. 8 080.00</span></h4>
              <h4 class="spec">&nbsp;</h4>
              <h4 class="pricewide">&nbsp;</h4>
          </div>
          <!-- entry end -->



       <div class="entry">
          <h2>NordLED 21600 LS</h2>
            <img src="../images/produkter/3001135w.jpg" class="left" style="border: 0;" />
            <p class="productinfo">
            <img src="../images/produkter/3001135-1.jpg" style="padding-right: 20px;">
            Multivolt arbeidslampe 21600lm. 24x10W.
            <br />
            Forbruk ved 12V 13.4A, forbruk ved 24V 6.7A.
            <br /><br />
            Mål (LxBxH): 1051x90x96mm.
            <br><br>
            Komplett med relé, lang kabel og bryter.
             </p>
              <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header">Pris</span></h4>
              <h4 class="pricesmall"><span class="art">3001023</span><span class="color">12/24V&nbsp;&nbsp;IP67</span><span class="volt">kr. 11 080.00</span></h4>
            <h4 class="spec">&nbsp;</h4>
            <h4 class="pricewide">&nbsp;</h4>
        </div>
        <!-- entry end -->








            <div class="entryhalf">
                <h2>NordLED projektor arb./ryggelys</h2>
                  <p class="productinfo">
                  <img src="../images/produkter/3001-004.jpg" class="left" style="border: 0;" />
                  <img src="../images/produkter/3001005-1.jpg" class="left" style="border: 0;" />
                  Arbeidslys/ryggelys for krevende bruksområder, 3 LED 12V, forbruk 0.26Amp. Lampehus i aluminium, linse i hardplast. <br /> Dimensjon: 112x36x36mm.
                  </p>
                  <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                  <h4 class="pricesmall"><span class="art">3001004</span><span class="color">-</span><span class="volt">12V<span class="dim"></span></span></h4>
                  <h4 class="spec">-</h4><h4 class="price">kr. 960.00</h4>
                  <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
              </div>
              <!-- entry end -->



              <div class="entryhalf">
                  <h2>NordLED 600 oppladbar lampe</h2>
                    <p class="productinfo">
                    <img src="../images/produkter/3001024.jpg" class="left" style="border: 0;" />
                    Arbeidslys for krevende bruksområder, 5 LED 15W, lampe med 3 stillinger (fullt, dim og SOS signal). Brukstid 3-4 timer ved fulladet batteri. Ladetid maks 7.5 timer, kan lades opp til 500 ganger. Magnetisk feste. Leveres med adaptere for 12V plugg og 220V. <br />
                    </p>
                    <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header">Info</span></h4>
                    <h4 class="pricesmall"><span class="art">3001024</span><span class="color">600lm</span><span class="volt">10-30V<span class="dim"></span></span></h4>
                    <h4 class="spec">-</h4><h4 class="price">kr. 1 880.00</h4>
                    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
                </div>
                <!-- entry end -->







        </div><!-- content end-->
	</div>
</div>

<?php require("../include/footer.php");?>


<script type="text/javascript"> Cufon.now(); </script>

</body>
</html>
