<?php
$page = "hovedside";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LED-shop Norge</title>
<link href="../css/common.css" rel="stylesheet" type="text/css" />
<?php require("../include/javascripts.php");?>
</head>

<body>
<div id="wrap">

    <div id="page">
    	<?php require("../include/top.php");?>

        <div id="content">
			<div class="entry wide">
    	<div class="slideshow">
          <img src="../images/front/t5t8.png" />
          <img src="../images/front/3001001.png" />
          <img src="../images/front/ekstremt.png" />
          
				</div>


               <div class="entry wide">

               </div>









								<div class="frontsmall left">
									<div class="toptext">VAREBESTILLING</div>
									<p class="frontsplash">
											Bestille varer? <br/>
										Send oss en e-post <br />
					<span style="color: #333; font-size: 120%; font-weight: bold;"><a href="mailto:info@nordled.no">info@nordled.no</a></span>
										<br />
										eller ring<br />
					<span style="color: #333; font-size: 120%; font-weight: bold;">958 66 862</span><br />

										</p>
								</div>

								<div class="frontsmall right">
									<div class="toptext">SALGSPANT</div>
									<p class="frontsplash">

									LED-shop Norge har salgspant i levert vare <br />
									inntil kjøpesummen med tillegg av omkostninger er betalt i sin helhet jf. pantelovens §3-14 til §3-22.</p>

								</div>

            </div>
            <!-- entry end -->




		<div class="entry wide" style="text-align: center;">
        	<p><span style="font-size: 18px;">Vi søker forhandlere! Er du interessert, ta kontakt med oss på info@nordled.no</span></p>
					<br /><br />

        </div>


        </div><!-- content end-->
	</div>
</div>

<?php require("../include/footer.php");?>


<script type="text/javascript">
Cufon.now();
</script>
</body>
</html>
