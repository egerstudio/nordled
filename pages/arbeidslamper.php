<?php
$page = "arbeidslamper";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LED-shop Norge</title>
<link href="../css/common.css" rel="stylesheet" type="text/css" />
<?php require("../include/javascripts.php");?>
</head>
<body>
<div id="wrap">

    <div id="page">
    	<?php require("../include/top.php");?>

        <div id="content">
        	<h1>arbeidslamper</h1>
            <h2>alle priser er inklusiv merverdiavgift</h2>





          <!--<div class="entryhalf">
          	<h2>NordLED fasadelys 100W</h2>
         	  <p class="productinfo">
              <img src="../images/produkter/3001601.jpg" class="left" style="border: 0;" />
              Slank og fleksibel fasadebelysning i værbestandig utførelse.
              <br />
				100-277V, 1.2A
			</p>
              <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header" style="text-align: right;">Pris</span></h4>
             <h4 class="pricesmall"><span class="art">3001601</span><span class="color">sort</span><span class="volt" style="text-align: right;">1 980.00</span></h4>
             <h4 class="pricesmall"><span class="art">3001602</span><span class="color">hvit</span><span class="volt" style="text-align: right;">1 980.00</span></h4>
             <h4 class="spec">&nbsp;</h4>
          </div>


          <div class="entryhalf">
          	<h2>NordLED fasadelys 200W</h2>
         	  <p class="productinfo">
              <img src="../images/produkter/3001603.jpg" class="left" style="border: 0;" />
              Slank og fleksibel fasadebelysning i værbestandig utførelse.
              <br />
				100-277V, 2.3A
			</p>
              <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header" style="text-align: right;">Pris</span></h4>
             <h4 class="pricesmall"><span class="art">3001603</span><span class="color">sort</span><span class="volt" style="text-align: right;">2 480.00</span></h4>
             <h4 class="pricesmall"><span class="art">3001604</span><span class="color">hvit</span><span class="volt" style="text-align: right;">2 480.00</span></h4>
             <h4 class="spec">&nbsp;</h4>
          </div>

          <div class="entryhalf">
          	<h2>NordLED fasadelys 300W</h2>
         	  <p class="productinfo">
              <img src="../images/produkter/3001605.jpg" class="left" style="border: 0;" />
              Slank og fleksibel fasadebelysning i værbestandig utførelse.
              <br />
				100-277V, 3.4A
			</p>
              <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header" style="text-align: right;">Pris</span></h4>
             <h4 class="pricesmall"><span class="art">3001605</span><span class="color">sort</span><span class="volt" style="text-align: right;">3 080.00</span></h4>
             <h4 class="pricesmall"><span class="art">3001606</span><span class="color">hvit</span><span class="volt" style="text-align: right;">3 080.00</span></h4>
             <h4 class="spec">&nbsp;</h4>
          </div>


          <div class="entryhalf">
          	<h2>NordLED fasadelys 400W</h2>
         	  <p class="productinfo">
              <img src="../images/produkter/3001605.jpg" class="left" style="border: 0;" />
              Slank og fleksibel fasadebelysning i værbestandig utførelse.
              <br />
				100-277V, 4.5A
			</p>
              <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header" style="text-align: right;">Pris</span></h4>
             <h4 class="pricesmall"><span class="art">3001607</span><span class="color">sort</span><span class="volt" style="text-align: right;">3 880.00</span></h4>
             <h4 class="pricesmall"><span class="art">3001608</span><span class="color">hvit</span><span class="volt" style="text-align: right;">3 880.00</span></h4>
             <h4 class="spec">&nbsp;</h4>
          </div>

				-->

          <div class="entryhalf">
          	<h2>NordLED oppladbar 10W</h2>
         	  <p class="productinfo">
              <img src="../images/produkter/3001510.jpg" class="left" style="border: 0;" />
              Oppladbart arbeidslys i aluminium og herdet glass. IP65. 100-240V. Ladetid 5 timer gir 4 timers brukstid. Leveres med lader 220V og 12V sigarettuttak.
			</p>
              <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
             <h4 class="pricesmall"><span class="art">3001510</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 460.00</span></h4>
             <h4 class="spec">&nbsp;</h4>
          </div>


          <div class="entryhalf">
          	<h2>NordLED oppladbar 20W</h2>
          	  <p class="productinfo">
              <img src="../images/produkter/3001510.jpg" class="left" style="border: 0;" />
              Oppladbart arbeidslys i aluminium og herdet glass. IP65. 100-240V. Ladetid 5 timer gir 4 timers brukstid. Leveres med lader 220V og 12V sigarettuttak.
          	</p>
              <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
             <h4 class="pricesmall"><span class="art">3001520</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 920.00</span></h4>
             <h4 class="spec">&nbsp;</h4>
          </div>

          <div class="entryhalf">
          	<h2>NordLED oppladbar 30W</h2>
          	  <p class="productinfo">
              <img src="../images/produkter/3001510.jpg" class="left" style="border: 0;" />
              Oppladbart arbeidslys i aluminium og herdet glass. IP65. 100-240V. Ladetid 5 timer gir 4 timers brukstid. Leveres med lader 220V og 12V sigarettuttak.
          	</p>
              <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
             <h4 class="pricesmall"><span class="art">3001530</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 1 140.00</span></h4>
             <h4 class="spec">&nbsp;</h4>
          </div>





           <div class="entryhalf">
            	<h2>NordLED arbeidslys 10W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/3001210.jpg" class="left" style="border: 0;" />
                Arbeidslys 10W i aluminiumshus og herdet glass, værbestandig. IP65. Sammenlignet med tradisjonell arbeidsbelysning sparer man inntil 60% energi, levetid over 30.000 timer. RoHS og CE merket. Hvitt lys.<br  /><br  />
                Forbruk 0.11Amp, 100-240V.
                <br />
				650lm, 100&deg; spredning.
                <br /><br />


                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">3001210</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 520.00</span></h4>
               <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys_10w.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
            </div>

            <div class="entryhalf">
            	<h2>NordLED arbeidslys 20W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/4001210.jpg" class="left" style="border: 0;" />
               Arbeidslys 20W i aluminiumshus og herdet glass, værbestandig. IP65. Sammenlignet med tradisjonell arbeidsbelysning sparer man inntil 60% energi, levetid over 30.000 timer. RoHS og CE merket. Hvitt lys.<br  /><br  />
                Forbruk 0.23Amp, 100-240V.
                <br />
				1300lm, 100&deg; spredning.
                <br /><br />


                </p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">3001220</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 720.00</span></h4>
               <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys20w.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
            </div>


            <div class="entryhalf">
            	<h2>NordLED arbeidslys 30W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/4001215.jpg" class="left" style="border: 0;" />
                Arbeidslys 30W i aluminiumshus og herdet glass, værbestandig. IP65. Sammenlignet med tradisjonell arbeidsbelysning sparer man inntil 60% energi, levetid over 30.000 timer. RoHS og CE merket. Hvitt lys.<br  /><br  />
                Forbruk 0.4Amp, 100-240V.
                <br />
				2180lm, 100&deg; spredning.
                <br /><br />


                </p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">3001230</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 880.00</span></h4>
               <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys30w.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
            </div>




            <div class="entryhalf">
            	<h2>NordLED arbeidslys 50W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/4001215.jpg" class="left" style="border: 0;" />
                Arbeidslys 50W i aluminiumshus og herdet glass, værbestandig. IP65. Sammenlignet med tradisjonell arbeidsbelysning sparer man inntil 60% energi, levetid over 30.000 timer. RoHS og CE merket. Hvitt lys.<br  /><br  />
                Forbruk 0.75Amp, 100-240V.
                <br />
				3500lm, 100&deg; spredning.
                <br /><br />


                </p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">3001250</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 1 080.00</span></h4>
               <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys50w.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
            </div>


            <div class="entryhalf">
            	<h2>NordLED arbeidslys 70W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/4001220.jpg" class="left" style="border: 0;" />
                Arbeidslys 70W i aluminiumshus og herdet glass, værbestandig. IP65. Sammenlignet med tradisjonell arbeidsbelysning sparer man inntil 60% energi, levetid over 30.000 timer. RoHS og CE merket. Hvitt lys.<br  /><br  />
                Forbruk 0.8Amp, 100-240V.
                <br />
				4900lm, 100&deg; spredning.
                <br /><br />


                </p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">3001270</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 1 600.00</span></h4>
               <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys70w.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
            </div>




            <div class="entryhalf">
            	<h2>NordLED arbeidslys 100W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/4001220.jpg" class="left" style="border: 0;" />
                Arbeidslys 100W i aluminiumshus og herdet glass, værbestandig. IP65. Sammenlignet med tradisjonell arbeidsbelysning sparer man inntil 60% energi, levetid over 30.000 timer. RoHS og CE merket. Hvitt lys.<br  /><br  />
                Forbruk 1.2Amp, 100-240V.
                <br />
				7500lm, 100&deg; spredning.
                <br /><br />


                </p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">3001300</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 2 180.00</span></h4>
               <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys100w.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
            </div>


            <div class="entryhalf">
            	<h2>NordLED arbeidslys 120W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/3001-120.jpg" class="left" style="border: 0;" />
                Arbeidslys 120W i aluminiumshus og herdet glass, værbestandig. IP65. Sammenlignet med tradisjonell arbeidsbelysning sparer man inntil 60% energi, levetid over 30.000 timer. RoHS og CE merket. Hvitt lys.<br  /><br  />
                Forbruk 1.5Amp, 100-240V.
                <br />
				8830lm, 100&deg; spredning.
                <br /><br />


                </p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">3001120</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 2 800.00</span></h4>
               <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys120w.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
            </div>




            <div class="entryhalf">
            	<h2>NordLED arbeidslys 150W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/3001-150.jpg" class="left" style="border: 0;" />
                Arbeidslys 150W i aluminiumshus og herdet glass, værbestandig. IP65. Sammenlignet med tradisjonell arbeidsbelysning sparer man inntil 60% energi, levetid over 30.000 timer. RoHS og CE merket. Hvitt lys.<br  /><br  />
                Forbruk 1.8Amp, 100-240V.
                <br />
				10 500lm, 100&deg; spredning.
                <br /><br />


                </p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">3001150</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 3 200.00</span></h4>
               <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys150w.pdf">Last ned produktspesifikasjon (PDF)</a></h4>
            </div>



            <div class="entryhalf">
            	<h2>NordLED industrilampe 100W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/3001400.jpg" class="left" style="border: 0;" />
                Industrilampe med lang levetid, typisk brukt i fabrikker og lagerlokaler. IP65. Vann- og støvtett. Lampehus i aluminium, skjerm i glass.<br  /><br  />
                Forbruk 1.2Amp, 100-240V.
                <br />
				7 500lm, standard 45&deg; spredning. Kan leveres med 90&deg; eller 120&deg; spredning.
                <br /><br />


                </p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">3001400</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 2 200.00</span></h4>

            </div>

             <div class="entryhalf">
            	<h2>NordLED industrilampe 120W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/3001400.jpg" class="left" style="border: 0;" />
                Industrilampe med lang levetid, typisk brukt i fabrikker og lagerlokaler. IP65. Vann- og støvtett. Lampehus i aluminium, skjerm i glass.<br  /><br  />
                Forbruk 1.4Amp, 100-240V.
                <br />
				9 100lm, standard 45&deg; spredning. Kan leveres med 90&deg; eller 120&deg; spredning.
                <br /><br />


                </p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">3001420</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 2 600.00</span></h4>

            </div>

            <!-- <div class="entryhalf">
            	<h2>NordLED industrilampe 150W</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/3001400.jpg" class="left" style="border: 0;" />
                Industrilampe med lang levetid, typisk brukt i fabrikker og lagerlokaler. IP65. Vann- og støvtett. Lampehus i aluminium, skjerm i glass.<br  /><br  />
                Forbruk 1.8Amp, 100-240V.
                <br />
				9 700lm, standard 45&deg; spredning. Kan leveres med 90&deg; eller 120&deg; spredning.
                <br /><br />


                </p>
               <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Info</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">3001450</span><span class="color">100-240V</span><span class="volt" style="text-align: right;">kr. 2 900.00</span></h4>

            </div>
-->

            <div class="entryhalf">
            	<h2>NordLED 1400 FF</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001007-1.jpg" class="left" style="border: 0;" />
                Multivolt arbeidslampe 1400lm. 27W, forbruk ved 12V 1A, forbruk ved 24V 0.75A.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Lys</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001007 </span><span class="color">Flomlys</span><span class="volt">12-24V&nbsp;&nbsp;&nbsp;IP68, SAE J1455<span class="dim"></span></span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/3001007.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 980.00</h4>
            </div>

            <div class="entryhalf">
            	<h2>NordLED 1400 RF/RS</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001008-1.jpg" class="left" style="border: 0;" />
                Multivolt arbeidslampe 1400lm. 27W, forbruk ved 12V 1A, forbruk ved 24V 0.75A.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Lys</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001008 </span><span class="color">Flomlys</span><span class="volt">12-24V&nbsp;&nbsp;&nbsp;IP68, SAE J1455<span class="dim"></span></span></h4>
                <h4 class="pricesmall"><span class="art">3001009 </span><span class="color">Spotlys</span><span class="volt">12-24V&nbsp;&nbsp;&nbsp;IP68, SAE J1455<span class="dim"></span></span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/3001007.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 980.00</h4>
            </div>




            <div class="entryhalf">
            	<h2>NordLED arbeidslys 4-LED</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001001.jpg" class="left" style="border: 0;" />
                Multivolt arbeidslampe 900lm. 10W, forbruk ved 12V 0.9A, forbruk ved 24V 0.4A.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Lys</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001001</span><span class="color">Flomlys</span><span class="volt">12-24V&nbsp;&nbsp;&nbsp;IP67<span class="dim"></span></span></h4>
                <h4 class="pricesmall"><span class="art">3001002</span><span class="color">Spotlys</span><span class="volt">12-24V&nbsp;&nbsp;&nbsp;IP67<span class="dim"></span></span></h4>                <h4 class="spec"><a href="../produktspesifikasjon/nordled_3001001.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 690.00</h4>
            </div>
            <!-- entry end -->



          	<!--<div class="entryhalf">
            	<h2>NordLED R48 - 48W</h2>
              	<p class="productinfo">
                <img src="../images/produkter/r48.jpg" class="left" style="border: 0;" />
                Arbeidslys for svært krevende bruksområder. Høyintensitets LED-dioder som yter 4800lm med over 50 000 timers driftstid i et vær- og vibrasjonsbestandig lampehus av støpt aluminium. Svært driftssikker og et meget godt lys.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001115</span><span class="color">-</span><span class="volt">11-70V&nbsp;&nbsp;&nbsp;IP68 <span class="dim"></span></span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys_r48.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 2 000.00</h4>
            </div>
            <!-- entry end -->
            <!--
            <div class="entryhalf">
            	<h2>NordLED F32 - 32W</h2>
              	<p class="productinfo">
                <img src="../images/produkter/f32.jpg" class="left" style="border: 0;" />
                Arbeidslys for svært krevende bruksområder. Høyintensitets LED-dioder som yter 3200lm med over 50 000 timers driftstid i et vær- og vibrasjonsbestandig lampehus av støpt aluminium. Svært driftssikker og et meget godt lys.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001114</span><span class="color">-</span><span class="volt">11-70V &nbsp;&nbsp;&nbsp;IP68<span class="dim"></span></span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys_f32.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 1 800.00</h4>
            </div>
            <!-- entry end -->

           <!--
           <div class="entryhalf">
            	<h2>NordLED PAR36</h2>
              	<p class="productinfo">
                <img src="../images/produkter/par36.jpg" class="left" style="border: 0;" />
                Arbeidslys for svært krevende bruksområder. 10-50V og levetid på over 30.000 timer. Vann- og værbestandig lampehus, IP67. 1000lm.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001118</span><span class="color">-</span><span class="volt">10-50V &nbsp;&nbsp;&nbsp;IP67<span class="dim"></span></span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys_par36.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 1 150.00</h4>
            </div>
            <!-- entry end -->

            <div class="entryhalf">
            	<h2>NordLED arbeidslys 35W 7-LED</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001003.jpg" class="left" style="border: 0;" />
                10-30V arbeidslys, vanntett, 3500lm. Mål: 130x110x74mm. Sertifisert EMC (R10).
                </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001003</span><span class="color">-</span><span class="volt">10-30V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/3001003.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 1 300.00</h4>

            </div>



          <div class="entryhalf">
            	<h2>NordLED projektor arb./ryggelys</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001-004.jpg" class="left" style="border: 0;" />
                Arbeidslys/ryggelys for krevende bruksområder, 3 LED 12V, forbruk 0.26Amp. Lampehus i aluminium, linse i hardplast.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001004</span><span class="color">-</span><span class="volt">12V<span class="dim"></span></span></h4>
                <h4 class="spec">-</h4><h4 class="price">kr. 680.00</h4>
                <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
            </div>
            <!-- entry end -->







            <!--
          	<div class="entryhalf">
            	<h2>NordLED arbeidslys 6</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-039.jpg" class="left" style="border: 0;" />
                12/24V arbeidslys med 1 meter kabel.
                3715lx på 1-meters avstand.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001001</span><span class="color">-</span><span class="volt">12/24V <span class="dim"></span></span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys6_3001001.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 980.00</h4>
            </div>
            <!-- entry end -->
            <!--
            <div class="entryhalf">
            	<h2>NordLED arbeidslys 8</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-040.jpg" class="left" style="border: 0;" />
                12/24V arbeidslys med 1 meter kabel.
                4630lx på 1-meters avstand.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001002</span><span class="color">-</span><span class="volt">12/24V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys8_3001002.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 1 190.00</h4>
            </div>
            <!-- entry end -->

            <!--
            <div class="entryhalf">
            	<h2>NordLED arbeidslys 14</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-041.jpg" class="left" style="border: 0;" />
                12/24V arbeidslys med 1 meter kabel.
                8460lx på 1-meters avstand. <br />
<br />

                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001003</span><span class="color">-</span><span class="volt">12/24V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys14_3001003.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 1 680.00</h4>
            </div>
            <!-- entry end -->



            <div class="entryhalf">
            	<h2>NordLED  arbeidslys 5</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-038.jpg" class="left" style="border: 0;" />
                En liten men kraftig lampe med 5 stk LED, og minimalt strømforbruk. Multivolt 12/24V, hus i aluminium og hardplast.
                Leveres i sort.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001005</span><span class="color">-</span><span class="volt">12/24V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys_flood_5_3001005.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 699.00</h4>
            </div>
            <!-- entry end -->


            <div class="entryhalf">
            	<h2>NordLED  arbeidslys 9</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-037.jpg" class="left" style="border: 0;" />
                En liten men kraftig lampe med 9 stk LED, og minimalt strømforbruk. Multivolt 12/24V, hus i aluminium og hardplast.
                Leveres i sort. <br />
<br />

                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001006</span><span class="color">-</span><span class="volt">12/24V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys_flood_9_3001006.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 899.00</h4>
            </div>
            <!-- entry end -->





            <!--
            <div class="entryhalf">
            	<h2>NordLED arbeidslys R 12-48V</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001007.jpg" class="left" style="border: 0;" />
                En liten men kraftig lampe med 4 stk LED, minimalt strømforbruk. Lampehus i aluminium og hardplast. Leveres i sort. IP66. Forbruk ved 12V 0.9A, forbruk ved 48V 0.3A.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001007</span><span class="color">-</span><span class="volt">12-48V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys_r.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 980.00</h4>
            </div>
            <!-- entry end -->
            <!--
            <div class="entryhalf">
            	<h2>NordLED arbeidslys F 12-48V</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001008.jpg" class="left" style="border: 0;" />
                En liten men kraftig lampe med 4 stk LED, minimalt strømforbruk. Lampehus i aluminium og hardplast. Leveres i sort. IP66. Forbruk ved 12V 0.9A, forbruk ved 48V 0.3A.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001008</span><span class="color">-</span><span class="volt">12-48V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys_f.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 980.00</h4>
            </div>
            <!-- entry end -->

						<!--
            <div class="entryhalf">
                        	<h2>NordLED SY-5 LED&nbsp;&nbsp;12V</h2>
                          	<p class="productinfo">
                            <img src="../images/produkter/3001116.jpg" class="left" style="border: 0;" />
                            Arbeidslampe med 5 LED-punkter, 15W, 500lm. Lampehus i aluminium og feste i rustfritt stål. Vann-, vibrasjon- og støvbestandig.<br />
            <br />

                            </p>
                            <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                            <h4 class="pricesmall"><span class="art">3001117</span><span class="color">hvit</span><span class="volt">12V</span></h4>
                            <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys_3001117.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 480.00</h4>
                        </div>
                        <!-- entry end -->

												<!--
            <div class="entryhalf">
                        	<h2>NordLED SY-6 LED&nbsp;&nbsp;12V</h2>
                          	<p class="productinfo">
                            <img src="../images/produkter/3001115.jpg" class="left" style="border: 0;" />
                            Arbeidslampe med 6 LED-punkter, 15W, 500lm. Lampehus i aluminium og feste i rustfritt stål. Vann-, vibrasjon- og støvbestandig.<br />
            <br />

                            </p>
                            <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                            <h4 class="pricesmall"><span class="art">3001116</span><span class="color">hvit</span><span class="volt">12V</span></h4>
                            <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys_3001116.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 480.00</h4>
                        </div>
                        <!-- entry end -->

											-->

            <div class="entryhalf">
            	<h2>NordLED Multi 4F</h2>
              	<p class="productinfo">
                <img src="../images/produkter/nordledmulti4f.jpg" class="left" style="border: 0;" />
                 Multivolt arbeidslampe 1000lm. 4x3W LED, forbruk ved 13.8V 1A, forbruk ved 28V 0.5A.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001119</span><span class="color">-</span><span class="volt">12/24V<span class="dim"></span></span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_arbeidslys_4f.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 780.00</h4>
            </div>



            <div class="entryhalf">
            	<h2>NordLED Multi 4R</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001111.jpg" class="left" style="border: 0;" />
                Multivolt arbeidslampe med 4 LED-punkter på 3W, 1400lx.
                <br /><br />

                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001111</span><span class="color">-</span><span class="volt">10-30V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_3001111.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 680.00</h4>
            </div>
            <!-- entry end -->

             <div class="entryhalf">
            	<h2>NordLED Multi 6</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001112.jpg" class="left" style="border: 0;" />
                Multivolt arbeidslampe med 6 LED-punkter på 3W, 2200lx.


                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001112</span><span class="color">-</span><span class="volt">10-30V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_3001112.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 820.00</h4>
            </div>
            <!-- entry end -->

             <div class="entryhalf">
            	<h2>NordLED Multi 8</h2>
              	<p class="productinfo">
                <img src="../images/produkter/3001113.jpg" class="left" style="border: 0;" />
                                Multivolt arbeidslampe med 8 LED-punkter på 3W, 3600lx.


                </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">3001113</span><span class="color">-</span><span class="volt">10-30V</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_3001113.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 880.00</h4>
            </div>
            <!-- entry end -->












        </div><!-- content end-->
	</div>
</div>

<?php require("../include/footer.php");?>


<script type="text/javascript"> Cufon.now(); </script>

</body>
</html>
