<?php
$page = "trafikktunnel";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LED-shop Norge</title>
<link href="../css/common.css" rel="stylesheet" type="text/css" />
<?php require("../include/javascripts.php");?>
</head>
<body>
<div id="wrap">

    <div id="page">
    	<?php require("../include/top.php");?>

        <div id="content">
        	<h1>Trafikk- & tunnellys</h1>
            <h2>alle priser er inklusiv merverdiavgift</h2>




            <!--<div class="entry">
            	<h2>NordLED søkelys LED</h2>
              	<p class="productinfo">
                <img src="../images/produkter/4001300.jpg" class="left" style="border: 0;" />
                Ekstremt kraftig LED søkelys, lyser opp mot 900 meter. Leveres i hvit eller svart utførelse. 370&deg; rotasjon, tilt 135&deg;. 12V 3Amp.
                </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">4001012</span><span class="color">-</span><span class="volt">12V</span></h4>
                <h4 class="spec">-</h4><h4 class="pricewide">kr. 5 200.00/stk</h4>
            </div>
            <!-- entry end -->


            <!--
            <div class="entryhalf">
             	<h2>NordLED 60W bensinstasjonbelysning</h2>
            	  <p class="productinfo">
                 <img src="../images/produkter/4001020.jpg" class="left" style="border: 0;" />
                 Høyytelses stasjonstakbelysning med lang levetid. 120&deg; spredning. 5750lm. IP67.
                 <br /><br />

                 <br />

                 </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header" style="width: 150px;">Info</span><span class="volt header" style="text-align: right; width: 140px;">Pris</span></h4>
                 <h4 class="pricesmall"><span class="art">4001020</span><span class="color" style="width: 150px;">hvit (5700-6500K)</span><span class="volt" style="text-align: right;  width: 140px;">kr. 4.400,-</span></h4>
             </div>


             <div class="entryhalf">
              	<h2>NordLED 80W bensinstasjonbelysning</h2>
             	  <p class="productinfo">
                  <img src="../images/produkter/4001025.jpg" class="left" style="border: 0;" />
                  Høyytelses stasjonstakbelysning med lang levetid. 120&deg; spredning. 7930lm. IP 67.

                  <br /><br />

                  <br />

                  </p>
                  <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header" style="width: 150px;">Info</span><span class="volt header" style="text-align: right; width: 140px;">Pris</span></h4>
                  <h4 class="pricesmall"><span class="art">4001025</span><span class="color" style="width: 150px;">hvit (5700-6500K)</span><span class="volt" style="text-align: right;  width: 140px;">kr. 4.800,-</span></h4>
              </div>
              -->


              <div class="entry">
               	<h2>NordLED bensinstasjonsbelysning</h2>
                 	<p class="productinfo">
                   <img src="../images/produkter/4001030.jpg" class="left" style="border: 0;" />
                   Spesiallampe for industri, bensinstasjoner, takkonstruksjoner og andre steder hvor innfelte lamper benyttes. 90W, 5000-5800K, ca. 8400lm. IP67. Arbeidstemperatur -30 - 50&deg;C. Levetid ca. 50.000 timer.
                   </p>
                    <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                   <h4 class="pricesmall"><span class="art">4001030</span><span class="color">-</span><span class="volt">100-240V</span></h4>
                   <h4 class="spec"><a href="#">&nbsp;</a></h4><h4 class="pricewide">kr. 5.500,-</h4>
               </div>
               <!-- entry end -->


               <div class="entry">
                	<h2>NordLED gate- og gårdslys L-30</h2>
                  	<p class="productinfo">
                    <img src="../images/produkter/4001040.jpg" class="left" style="border: 0;" />
                    Gate-, gårds- og fortausbelysning, også ideel for løyper og turstier. Enkel design forenkler vedlikehold og installasjon. Høy ytelse og lang levetid. Arbeidstemperatur -30 - 50&deg;C. Levetid opp mot 50.000 timer. 100-240V. IP67.
                    Feste på mast kan reguleres i diameter fra 40-60mm.
                    </p>
                     <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                    <h4 class="pricesmall"><span class="art">4001030</span><span class="color">-</span><span class="volt">100-240V</span></h4>
                    <h4 class="spec"><a href="#">&nbsp;</a></h4><h4 class="pricewide">kr. 3.200,-</h4>
                </div>
                <!-- entry end -->


                <!--
                <div class="entry">
                 	<h2>NordLED fasade- og områdebelysning (LP1, LP2, LP3 &amp; LP4)</h2>
                   	<p class="productinfo">
                     <img src="../images/produkter/4001050.jpg" class="left" style="border: 0;" />
                     Lamper ideele for fasade- og områdebelysning som parkeringsplasser og kjøpesenter. Høy ytelse og lang levetid opp mot 100.000 timer. IP67. Arbeidstemperatur -30 - 50&deg;C. 5000K. 100-240V.
                     </p>
                      <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Mål</span><span class="volt header" style="text-align: right;">Pris</span></h4>
                     <h4 class="pricesmall"><span class="art">LP1 - 4001050</span><span class="color">471x420x143mm (9kg)</span><span class="volt" style="font-weight: bold; text-align: right;">kr. 3 040.00</span></h4>
                     <h4 class="pricesmall"><span class="art">LP2 - 4001055</span><span class="color">579x420x143mm (11kg)</span><span class="volt" style="font-weight: bold; text-align: right;">kr. 3 940.00</span></h4>
                     <h4 class="pricesmall"><span class="art">LP3 - 4001060</span><span class="color">687x420x143mm (15kg)</span><span class="volt" style="font-weight: bold; text-align: right;">kr. 4 980.00</span></h4>
                     <h4 class="pricesmall"><span class="art">LP4 - 4001065</span><span class="color">795x420x143mm (17kg)</span><span class="volt" style="font-weight: bold; text-align: right;">kr. 5 780.00</span></h4>


                 </div>
                 -->
                 <!-- entry end -->






           <div class="entry">
            	<h2>NordLED Atlas bensinstasjonsbelysning</h2>
              	<p class="productinfo">
                <img src="../images/produkter/4001400.jpg" class="left" style="border: 0;" />
                Spesiallampe for industri, bensinstasjoner, takkonstruksjoner og andre steder hvor innfelte lamper benyttes. Aluminiumskasse, herdet glass 5mm, levetid opp til 70.000 timer. IP65. Sertifisert ATEX (direktiv 94/9/EC).
                </p>
                 <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">4001013</span><span class="color">-</span><span class="volt">230V</span></h4>
                <h4 class="spec">-</h4><h4 class="pricewide">kr. 5 580.00/stk</h4>
            </div>
            <!-- entry end -->



          <!--
           <div class="entryhalf">
            	<h2>NordLED 100W tunnellys</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/4001200.jpg" class="left" style="border: 0;" />
                Høyytelses tunnelbelysning med levetid på opptil 50.000 timer og energibesparing opp mot 80% mot tradisjonell natriumbelysning. Ingen forsinkelse eller flimmer, konstant lys og lav temperatur.  Lampehus i aluminium med herdet glass. IP65.
                <br /><br />
                Arbeidstemperatur mellom -20&deg;C og +40&deg;C. Vekt: 8.4kg. Leveres i grå eller svart utførelse. Mål: 369x294x226mm.
                <br /><br />

                <br />

                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header" style="width: 150px;">Info</span><span class="volt header" style="text-align: right; width: 140px;">Pris</span></h4>
                <h4 class="pricesmall"><span class="art">4001008</span><span class="color" style="width: 150px;">hvit (5500-7000K)</span><span class="volt" style="text-align: right;  width: 140px;">kr. 4 200.00</span></h4>
                <h4 class="pricesmall"><span class="art">4001009</span><span class="color" style="width: 150px;">nøytral (4000-4500K)</span><span class="volt" style="text-align: right; width: 140px;">kr. 4 200.00</span></h4>
                <h4 class="pricesmall"><span class="art">4001010</span><span class="color" style="width: 150px;">varmhvit (2700-3200K)</span><span class="volt" style="text-align: right; width: 140px;">kr. 4 200.00</span></h4>
                 <h4 class="spec"><a href="../produktspesifikasjon/nordled100wtunnel.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">-</h4>
            </div>





           <div class="entryhalf">
            	<h2>NordLED trafikkdirigent 260RG</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/nytraffikdirigent.jpg" class="left" style="border: 0;" />
                Konstant rødt eller grønt lys eller rød blink, opp til 36 timers levetid per sett 3xAA-batterier, 1.5V. Lengde: 260mm, diameter: 39mm, 190gr. Magnetefeste i bunn. Synlig på 200 meters avstand i mørket.
                <br /><br />
<br /><br />

<br /><br /><br />
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header" style="text-align: right;">Pris</span></h4>
                <h4 class="pricesmall"><span class="art">4001011</span><span class="color">rød/grønn</span><span class="volt" style="text-align: right;">kr. 500.00</span></h4>
                <br /><br /><br /><br /><br /><br /><br />
            </div>
            -->


						<!--
            <div class="entryhalf">
            	<h2>NordLED refleksgenser klasse 3</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/4001100.jpg" class="left" style="border: 0;" />
                Klasse-3 refleksgenser i fleece. <br />
                Størrelser: S/M, L/XL, 2XL/3XL, 4XL/5XL.
                <br /><br />

                <br />

                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">4001100</span><span class="color">gul</span><span class="volt" style="text-align: right;">kr. 360.00</span></h4>

            </div>


            <div class="entryhalf">
            	<h2>NordLED refleksvest klasse 3</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/4001105.jpg" class="left" style="border: 0;" />
                Klasse-3 refleksvest i tynt overtrekksstoff.<br />
				Størrelser: S/M, L/XL, 2XL/3XL, 4XL/5XL.
                <br /><br />

                <br />

                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header" style="text-align: right;">Pris</span></h4>
               <h4 class="pricesmall"><span class="art">4001105</span><span class="color">gul</span><span class="volt" style="text-align: right;">kr. 210.00</span></h4>
            </div>
					-->
            <!--
            <div class="entryhalf">
            	<h2>NordLED trafikkdirigent R</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/4001050.jpg" class="left" style="border: 0;" />
                Konstant lys eller 120 blink per minutt, opp til 300 timers levetid per sett D-batterier. 55cm lang, diameter 44mm.
                <br /><br />

                <br />

                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">4001050</span><span class="color">rød</span><span class="volt">-</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_trafikkdirigent_r.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 700.00</h4>
            </div>

            <div class="entryhalf">
            	<h2>NordLED trafikkdirigent RG</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/4001051.gif" class="left" style="border: 0;" />
                Konstant lys eller 120 blink per minutt. Velg mellom rød & grønn blinkende eller konnstant lys. Opp til 300 timers levetid per sett D-batterier. 55cm lang, diameter 44mm.
                <br />
                <br />

                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">4001051</span><span class="color">rød & grønn</span><span class="volt">-</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_trafikkdirigent_rg.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 800.00</h4>
            </div>
					-->

            <div class="entry">
            	<h2>NordLED 140 varslingslys</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-056.jpg" class="left" style="border: 0;" />
                12V lamper ideelt for bruk på industriporter, vegbommer og andre steder man ønsker å varsle.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">4001001</span><span class="color">rød</span><span class="volt">12V</span></h4>
                <h4 class="pricesmall"><span class="art">4001002</span><span class="color">gul</span><span class="volt">12V</span></h4>
                <h4 class="pricesmall"><span class="art">4001003</span><span class="color">grønn</span><span class="volt">12V</span></h4>
                <h4 class="spec">-</h4><h4 class="pricewide">kr. 520.00/stk</h4>
            </div>
            <!-- entry end -->

            
           <!--
            <div class="entryhalf">
            	<h2>NordLED SD48</h2>
              	<p class="productinfo">
                <img src="../images/produkter/2004-019.jpg" class="left" style="border: 0;" />
                Svært lyssterk lampe for tunnellys og arbeidsbelysning, tilsvarer 250W HPS-pære. Levetid på 50 000 timer og svært lav varmeutvikling. IP65.
                LBH: 320x320x250mm. Vekt: 9.5kg.<br />
				Arbeidstemperatur: -30&deg;C til +50&deg;C.
                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">4001010</span><span class="color">hvit</span><span class="volt">60W 100-240V&nbsp;&nbsp;&nbsp;5000lm</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_sd48_4001010.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 4 300.00</h4>
            </div>
            <!-- entry end -->
           <!--
            <div class="entryhalf">
            	<h2>NordLED SD96</h2>
           	  <p class="productinfo">
                <img src="../images/produkter/4001015.jpg" class="left" style="border: 0;" />
                Svært lyssterk lampe for tunnellys og arbeidsbelysning. Levetid på 50 000 timer og svært lav varmeutvikling. IP65.
                LBH: 660x320x250mm. Vekt: 15kg. Arbeidstemperatur: -30&deg;C til +50&deg;C.
                <br />
                <br />

                </p>
                <h4 class="pricesmall"><span class="art header">Artikkelnr</span><span class="color header">Farge</span><span class="volt header">Info</span></h4>
                <h4 class="pricesmall"><span class="art">4001015</span><span class="color">hvit</span><span class="volt">120W 100-240V&nbsp;&nbsp;&nbsp;9400lm</span></h4>
                <h4 class="spec"><a href="../produktspesifikasjon/nordled_sd96_4001015.pdf">Last ned produktspesifikasjon (PDF)</a></h4><h4 class="price">kr. 7 800.00</h4>
            </div>
            <!-- entry end -->





        </div><!-- content end-->
	</div>
</div>

<?php require("../include/footer.php");?>


<script type="text/javascript"> Cufon.now(); </script>

</body>
</html>
