<?php
$page = "omled";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LED-shop Norge</title>
<link href="../css/common.css" rel="stylesheet" type="text/css" />
<?php require("../include/javascripts.php");?>
</head>
<body>
<div id="wrap">
	
    <div id="page">
    	<?php require("../include/top.php");?>
		
        <div id="content">
        	<h1>om LED</h1>
            <div class="entry">
	           <h2>Hva er LED?</h2>
               <p>
                Lysdioder er små elektroniske halvlederbrikker som sender ut lys når det går strøm gjennom dem. Strømstyrken bestemmer hvor mye lys de avgir. Her er det ingen glødetråd som i glødelamper eller gass som i lysrør.
                <br /><br />
I motsetning til en vanlig pære der en metalltråd varmes opp og begynner å gløde, forsvinner en svært liten del av energien i varme når en LED tilføres strøm. Foe å oppnå samme lyseffekt som en vanlig pære kan en si at en LED bare bruker ca. 20 prosent så mye energi som en vanlig pære for å oppnå samme lyseffekt. Dette vil blant annet være avhengig av mengde lys (lumen) og fargen på lyset (kelvin) som produseres. Siden en LED avgir lite varme kan flere dioder monteres sammen for å oppnå bedre lyseffekt.
				</p>
                <h2>Hvorfor LED?</h2>
                <p>
                LED er konstruert for opptil 80 % lavere energiforbruk enn normale glødepærer
                <br />
                En LED-pære på 5w vil i løpet av sin levetid på 50 000 timer forbruke 250 kw, eller 250 kroner, forutsatt en kw-pris på 1 krone (inkl. nettleie).<br />
                En vanlig 40w glødelampe, som er nødvendig for å gi samme lyseffekt som LED-pærene, vil i løpet av 50 000 timer ha forbrukt 2000kw, eller 2000 kroner. I tillegg til merkostnaden på 1500 kroner ville glødepæren ha blitt skiftet ut minst 20 ganger i løpet av den ene LED-pærens levetid.
                <br /><br />
                <strong>LED</strong><br />
				- har lang levetid (30.000 - 100.000 timer). 
                <br />
                - har lavere driftskostnader. 
                <br />
                - gir klare lysfarger, og noen kan dimmes
                <br />
                - tenner direkte
                <br />
                - tåler støt og rystelser
                <br />
                - inneholder ikke miljøgifter
                <br />
                - avgir ikke UV- eller IR-stråling
                <br />
                - har lav overflatetemperatur
                <br />
                - gir fullt lysutbytte ved start (trenger ingen forvarming)
                <br />
				- er velegnet i områder med lave omgivelsestemperaturer. I første rekke tenkes da på utebelysning.  
                <br />
				- har konstant fargetemperatur ved dimming     
                </p>

          	</div>
            <div class="entry">
            	<h2>Fakta om LED-lamper</h2>
                <p>I denne tabellen har vi sammenlignet ulike typer lamper</p>
            	<table cellspacing="0" class="fakta">
            		<tr>
                		<th></th>
                        <th  style="background-color: #86CF7A; color: #333;">LED-lampe</th>
                        <th>Sparepære</th>
                        <th>Halogenlampe</th>
                        <th>Glødelampe</th>
                    </tr>
                   	<tr>
                    	<th class="side">Levetid</th>
                        <td style="background-color: #86CF7A;">50 000</td>
                        <td>10 000</td>
                        <td>2 000</td>
                        <td>1 000</td>
                    </tr>
                    <tr>
                    	<th class="side">Watt</th>
                        <td style="background-color: #86CF7A;">5W</td>
                        <td>7W</td>
                        <td>40W</td>
                        <td>40W</td>
                    </tr>
                    <tr>
                    	<th class="side">Energiforbruk for 50 000 timer/kWh</th>
                        <td style="background-color: #86CF7A;">200</td>
                        <td>350</td>
                        <td>2 000</td>
                        <td>2 000</td>
                    </tr>
                </table>
                <h2>Effekt</h2>
                <table cellspacing="0" class="fakta">
            		<tr>
                		<th class="side">Lyskilde</th>
                        <th colspan="4">Effekt</th>
                    </tr>
                   	<tr>
                    	<th class="side">Glødelampe / halogen</th>
                        <td>25W</td>
                        <td>40W</td>
                        <td>60W</td>
                        <td>100W</td>
                    </tr>
                    <tr>
                    	<th class="side">Sparepære</th>
                        <td>7W</td>
                        <td>11W</td>
                        <td>15W</td>
                        <td>25W</td>
                    </tr>
                    <tr>
                    	<th class="side"  style="background-color: #86CF7A; color: #333;">LED</th>
                        <td style="background-color: #86CF7A;">3W</td>
                        <td style="background-color: #86CF7A;">5W</td>
                        <td style="background-color: #86CF7A;">8W</td>
                        <td style="background-color: #86CF7A;">13W</td>
                    </tr>
                </table>
            </div>
            
        </div><!-- content end-->
	</div>
</div>

<?php require("../include/footer.php");?>


<script type="text/javascript"> Cufon.now(); </script>

</body>
</html>
