<?php
$page = "messe";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LED-shop Norge</title>
<link href="../css/common.css" rel="stylesheet" type="text/css" />
<?php require("../include/javascripts.php");?>
</head>
<body>
<div id="wrap">
	
    <div id="page">
    	<?php require("../include/top.php");?>
		
        <div id="content">
        	<h1>Diverse</h1>
            <div class="entry">
                 
	           <h2>Transportmessa 2013 på Gardermoen</h2>
               <p>
               <strong>Besøk oss på Transportmessa på Gardermoen fra 5-7. september 2013!</strong>
               <br />
               For mer informajson, besøk <a href="http://www.transportmessa.no/2013/index.html" target="_blank">Tranportmessa på nett</a>.

          	</div>
            
            
         
            <div class="entry">
                 
	           <h2>Referanser</h2>
               <p>
               <strong>Her legger vi snart ut bilder fra noen av våre prosjekter.</strong>
               
          	</div>
            
            
             
        </div><!-- content end-->
	</div>
</div>

<?php require("../include/footer.php");?>


<script type="text/javascript"> Cufon.now(); </script>

</body>
</html>
